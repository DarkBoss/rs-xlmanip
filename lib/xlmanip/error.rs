use crate::query::parse::errors::*;

#[derive(Debug)]
pub enum Error {
  Init(String),
  IO(std::io::Error),
  Parse{expr: String, path: Option<String>},
  NotImplementedYet,
  Unknown(String),
}

impl std::fmt::Display for Error {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{}", match self {
      Error::Init(s) => format!("init: {}", s),
      Error::NotImplementedYet => format!("not implemented yet"),
      Error::Parse{expr, path} => format!("{}: parse: {}", match path {
        Some(p) => p.to_string(),
        None => String::from("None"),
      }, expr),
      Error::IO(e) => format!("I/O: {}", e.to_string()),
      Error::Unknown(s) => format!("unknown: {}", s),
    })
  }
}


impl std::error::Error for Error {
  fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
      match self {
        Error::IO(e) => Some(e),
        _ => None,
      }
  }

}