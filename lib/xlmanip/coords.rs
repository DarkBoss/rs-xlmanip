#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Size {
  pub width: usize,
  pub height: usize,
}

impl Size {
  pub fn new() -> Size { Size::with_values(0, 0) }
  pub fn with_values(x: usize, y: usize) -> Size { Size { width: x, height: y } }
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Point {
  pub x: usize,
  pub y: usize,
}

impl Point {
  pub fn new() -> Point { Point::with_values(0, 0) }
  pub fn with_values(x: usize, y: usize) -> Point { Point { x, y } }
}