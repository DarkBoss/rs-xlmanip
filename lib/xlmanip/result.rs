use crate::error::Error;

pub type Result<V> = std::result::Result<V, Error>;