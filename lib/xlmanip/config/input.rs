
#[derive(Debug, Clone)]
pub enum AppInput {
  File(String),
  Stdin,
}

impl std::fmt::Display for AppInput {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{}", match self {
      AppInput::File(s) => s.clone(),
      AppInput::Stdin => format!("{}", "stdin")
    })
  }
}
