use crate::fmt::Writer;
use crate::error::Error;
use crate::result::Result;

#[derive(Clone)]
pub enum AppOutput {
  File(String),
  Stdout,
  Stderr,
  Custom(std::rc::Rc<std::cell::RefCell<dyn std::io::Write>>)
}

impl std::fmt::Debug for AppOutput {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{}", self.kind())
  }
}
impl AppOutput {
  pub fn create(&self) -> Result<Writer> {
    Ok(match &self {
      AppOutput::File(name) => {
        let p = std::path::Path::new(&name);
        match p.parent() {
          Some(n) => {
            std::fs::create_dir_all(n).or_else(|e| Err(Error::IO(e)))?;
            Ok(())
          }
          None => Ok(())
        }?;
        let f = std::fs::File::create(&name).or_else(|e| Err(Error::IO(e)))?;
        std::rc::Rc::new(std::cell::RefCell::new(f))
      }
      AppOutput::Stderr => std::rc::Rc::new(std::cell::RefCell::new(std::io::stderr())),
      AppOutput::Stdout => std::rc::Rc::new(std::cell::RefCell::new(std::io::stdout())),
      AppOutput::Custom(w) => w.clone()
    })
  }

  pub fn kind(&self) -> String {
    format!("{}", match self {
      AppOutput::File(_) => "file",
      AppOutput::Stdout => "stdout",
      AppOutput::Stderr => "stderr",
      AppOutput::Custom(_) => "custom",
    })
  }

  pub fn url(&self) -> String {
    match self {
      AppOutput::File(s) => format!("{}://{}", self.kind(), s),
      AppOutput::Stdout => format!("{}://", self.kind()),
      AppOutput::Stderr => format!("{}://", self.kind()),
      AppOutput::Custom(_) => format!("{}://", self.kind())
    }
  }
}

impl std::fmt::Display for AppOutput {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{}", self.url())
  }
}
