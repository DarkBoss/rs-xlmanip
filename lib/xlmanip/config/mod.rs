mod input;
mod output;
mod parsers;

use crate::fmt::Serializer;

pub use input::*;
pub use output::*;
pub use parsers::*;

use super::verbose::VerboseLevel;
use super::query::filter::Filter;
use super::error::Error;
use super::result::Result;
use log::*;

const DEFAULT_LOG_CONFIG: &'static str = "log4rs.yml";

#[derive(Debug, Clone)]
pub struct Config {
  pub inputs: Vec<AppInput>,
  pub output: AppOutput,
  pub force: bool,
  pub mode: Option<String>,
  pub format: Option<String>,
  pub filter: Option<Filter>,
  pub verbose: VerboseLevel,
  pub sheets: Vec<String>
}

impl std::default::Default for Config {
  fn default() -> Config {
    Config {
      inputs: vec![AppInput::Stdin],
      output: AppOutput::Stdout,
      force: false,
      filter: None,
      format: None,
      mode: None,
      verbose: VerboseLevel::Normal,
      sheets: vec![],
    }
  }
}

use std::sync::{Arc, Mutex, LockResult, MutexGuard};

pub type SharedConfig = Arc<Mutex<Config>>;

// impl std::default::Default for SharedConfig {
//   fn default() -> SharedConfig {
//     SharedConfig(Arc::new(Mutex::new(Config::default())))
//   }
// }

static mut g_cfg_inst: Option<SharedConfig> = None;

impl Config {
  pub fn get() -> Config {
    unsafe {
      if g_cfg_inst.is_none() {
        g_cfg_inst = Some(Arc::new(Mutex::new(Config::default())));
      }
      let local_cfg = g_cfg_inst.clone().unwrap().clone();
      let cfg = local_cfg.lock().or_else(|e| Err(Error::Unknown(e.to_string())));
      use std::ops::Deref;
      let mut ret: Config = Config::default();
      match cfg {
        Err(e) => panic!("config: {}", e.to_string()),
        Ok(v) => {
          ret = v.clone();
        }
      };
      ret
    }
  }

  pub fn get_shared() -> SharedConfig {
    unsafe {
      if g_cfg_inst.is_none() {
        g_cfg_inst = Some(Arc::new(Mutex::new(Config::default())));
      }
      g_cfg_inst.clone().unwrap().clone()
    }
  }

  pub fn set(c: Config) {
    unsafe {
      g_cfg_inst = Some(Arc::new(Mutex::new(c)));
    }
  }
}