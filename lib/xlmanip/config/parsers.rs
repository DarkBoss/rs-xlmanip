use super::{SharedConfig, Config};
use crate::app::{get_serializer, App};
use crate::config::{AppInput, AppOutput};
use crate::verbose::VerboseLevel;
use crate::result::Result;
use crate::filter::Filter;
use crate::expr::Expr;
use crate::error::Error;

use log::*;

fn parse_inputs(matches: &clap::ArgMatches<'_>, cfg: &mut Config) -> Result<()> {
  if matches.is_present("INPUT_FILE") {
    cfg.inputs.clear();
    for input in matches.values_of("INPUT_FILE").unwrap() {
      if !std::path::Path::new(input).exists() {
        return Err(Error::Init(format!(
          "{}: invalid input file, does not exist",
          input
        )));
      }  
      cfg.inputs.push(AppInput::File(String::from(input)));
    }
  }
  Ok(())
}

fn parse_sheet(matches: &clap::ArgMatches<'_>, cfg: &mut Config) -> Result<()> {
  if matches.is_present("SHEET_NAME") {
    for value in matches.values_of("SHEET_NAME").unwrap() {
      cfg.sheets.push(value.to_string());
    }
  }
  Ok(())
}

fn parse_mode(matches: &clap::ArgMatches<'_>, cfg: &mut Config) -> Result<()> {
  for ref mode in crate::mode::all_modes() {
    if let Some(cmd) = matches.subcommand_matches(mode.name()) {
      cfg.mode = Some(mode.name().clone());
      return Ok(())
    }
  }
  Err(Error::Unknown(format!("{}", "no app mode")))
}

fn parse_verbose(matches: &clap::ArgMatches<'_>, cfg: &mut Config) -> Result<()> {
  if matches.is_present("verbose") {
    cfg.verbose = VerboseLevel::Normal;
  }
  if matches.is_present("silent") {
    cfg.verbose = VerboseLevel::Silent;
  }
  if matches.is_present("talkative") {
    cfg.verbose = VerboseLevel::Talkative;
  }
  if matches.is_present("quiet") {
    cfg.verbose = VerboseLevel::Discreet;
  }
  match cfg.verbose {
    VerboseLevel::Silent => log::set_max_level(log::LevelFilter::Off),
    VerboseLevel::Discreet => log::set_max_level(log::LevelFilter::Warn),
    VerboseLevel::Normal => log::set_max_level(log::LevelFilter::Info),
    VerboseLevel::Talkative => log::set_max_level(log::LevelFilter::Trace),
  }
  Ok(())
}

pub fn parse_options() -> Result<SharedConfig> {
  use clap::{App as CliApp, Arg as CliArg, ArgGroup as CliArgGroup};
  let mut cfg = Config::default();
  let mut cliApp = CliApp::new(env!("CARGO_PKG_NAME"))
    .version(env!("CARGO_PKG_VERSION"))
    .author("Morgan W. <welschmorgan@gmail.com>")
    .about("Allows CLI manipulation of spreadsheet files")
    .arg(
      CliArg::with_name("output")
        .help("set output file")
        .short("o")
        .long("output")
        .value_name("FILE")
        .takes_value(true)
    )
    .arg(
      CliArg::with_name("force")
        .help("overwrite output files")
        .short("f")
        .long("force")
    )
    .arg(
      CliArg::with_name("output-format")
        .help("set output format")
        .short("F")
        .long("format")
        .value_name("FORMAT")
        .takes_value(true)
    )
    .arg(
      CliArg::with_name("filter")
        .help("filter the ouput results")
        .long("filter")
        .value_name("FILTER_EXPR")
        .takes_value(true)
    )
    .arg(
      CliArg::with_name("input-opt")
        .help("set input spreadsheet")
        .short("i")
        .long("input")
        .value_name("FILE")
        .takes_value(true)
        .group("INPUT_FILE")
    )
    .arg(
      CliArg::with_name("INPUT")
        .help("set input workbook")
        .value_name("INPUT")
        .takes_value(true)
        .group("INPUT_FILE")
    )
    .arg(
      CliArg::with_name("SHEET")
        .help("filter worksheets by name")
        .value_name("SHEET")
        .takes_value(true)
        .group("SHEET_NAME")
    )
    .arg(
      CliArg::with_name("sheet")
        .help("filter worksheets by name")
        .short("S")
        .long("sheet")
        .value_name("NAME")
        .takes_value(true)
        .group("SHEET_NAME")
    )
    .arg(
      CliArg::with_name("silent")
        .help("show no log messages")
        .short("s")
        .long("silent")
    )
    .arg(
      CliArg::with_name("quiet")
        .help("show few log messages")
        .short("q")
        .long("quiet")
    )
    .arg(
      CliArg::with_name("verbose")
      .help("show more log messages")
        .short("v")
        .long("verbose")
      )
    .arg(
      CliArg::with_name("talkative")
        .help("show all log messages")
        .long("talkative")
    );
  for ref mode in crate::mode::all_modes() {
    let local_mode = mode.clone();
    cliApp = cliApp.subcommand(
      CliApp::new(local_mode.name())
    );
  }
  let matches = cliApp.get_matches();
  parse_inputs(&matches, &mut cfg)?;
  parse_output(&matches, &mut cfg)?;
  parse_verbose(&matches, &mut cfg)?;
  parse_force(&matches, &mut cfg)?;
  parse_mode(&matches, &mut cfg)?;
  parse_sheet(&matches, &mut cfg)?;
  parse_filter(&matches, &mut cfg)?;
  parse_format(&matches, &mut cfg)?;
  debug!("Loaded configuration: {:#?}", cfg);
  Config::set(cfg);
  Ok(Config::get_shared())
}

pub fn parse_force(matches: &clap::ArgMatches<'_>, cfg: &mut Config) -> Result<()> {
  if matches.is_present("force") {
    cfg.force = true;
  }
  Ok(())
}
pub fn parse_output(matches: &clap::ArgMatches<'_>, cfg: &mut Config) -> Result<()> {
  if matches.is_present("output") {
    let fname = matches.value_of("output").expect("failed to get the value of output option").to_string();
    cfg.output = AppOutput::File(format!("{}", &fname));
  }
  Ok(())
}

pub fn parse_format(matches: &clap::ArgMatches<'_>, cfg: &mut Config) -> Result<()> {
  if matches.is_present("output-format") {
    cfg.format = Some(matches.value_of("output-format").expect("failed to get the value of output-format option").to_string()); 
  } else {
    cfg.format = Some("csv".to_string());
  }
  if get_serializer(cfg.clone().format.as_ref().unwrap()).is_none() {
    return Err(Error::Init(format!("unknown format '{}'", cfg.format.as_ref().unwrap())))
  }
  Ok(())
}

pub fn parse_filter(matches: &clap::ArgMatches<'_>, cfg: &mut Config) -> Result<()> {
  if matches.is_present("filter") {
    let expr_s = matches.value_of("filter").unwrap();
    let expr = Expr::parse(expr_s.to_string())?;
    cfg.filter = Some(Filter::from(expr));
  }
  Ok(())
}

pub fn init_logging() -> Result<log4rs::Handle> {
  use std::fs::File;
  use std::io::Write;
  let path = std::path::Path::new("log4rs.yml");
  let content: &str = include_str!("../log4rs.yml");
  if !path.exists() {
    let display = path.display();

    // Open a file in write-only mode, returns `io::Result<File>`
    let mut file = File::create(&path)
      .or_else(|e| Err(
        Error::Init(format!("{}: failed to create file: {}", display, e.to_string()))
      )
    )?;

    file.write_all(content.as_bytes())
      .or_else(|e| Err(
        Error::Init(format!("{}: {}", display, e.to_string()))
      )
    )?;
    eprintln!("successfully wrote to {}", display);
  }
  let config = log4rs::load_config_file(path.as_os_str(), Default::default()).or_else(|e| Err(Error::Init(e.to_string())))?;
  Ok(log4rs::init_config(config).or_else(|e| Err(Error::Init(e.to_string())))?)
}
