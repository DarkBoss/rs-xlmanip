use crate::fmt::Writer;
use std::collections::HashMap;
use crate::book::BookStore;
use crate::error::Error;
use super::result::ModeResult;
use crate::app::App;
use crate::config::{Config as AppConfig, AppOutput};
use crate::fmt::{Config as SerializerConfig};

// The mode macro allows quick import of app modes
#[macro_export]
macro_rules! mode {
  ( $($x:ident),* ) => {
    $(
      mod $x; pub use $x::*;
    )*
  };
}

pub trait Mode {
  fn name(&self) -> &String;
  fn about(&self) -> &String;

  fn short_flag(&self) -> &Option<String>;
  fn long_flag(&self) -> &Option<String>;

  fn process(&self, app: &mut App) -> ModeResult;
}


use std::sync::{Arc, Mutex};

pub type SharedMode = Arc<Mutex<dyn Mode>>;