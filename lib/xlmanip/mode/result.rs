use crate::book::Book;
use crate::result::Result;
use crate::range::Range;
use std::collections::HashMap;

pub type ModeResult = Result<()>;