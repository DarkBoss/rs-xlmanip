pub struct ModeData {
  pub name: String,
  pub about: String,
  pub short_flag: Option<String>,
  pub long_flag: Option<String>
}