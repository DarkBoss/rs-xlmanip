use super::super::{Mode, ModeResult, ModeData};
use super::super::super::app::App;
use crate::error::Error;

pub struct ExtractSheet(ModeData);

impl ExtractSheet {
  pub fn new() -> ExtractSheet {
    ExtractSheet(ModeData {
      name: "extract-sheet".to_string(),
			about: "".to_string(),
      short_flag: Some("xs".to_string()),
      long_flag: Some("extract-sheet".to_string()),
    })
  }
}

impl Mode for ExtractSheet {
  fn name(&self) -> &String {
    &self.0.name
	}
  
  fn about(&self) -> &String {
    &self.0.name
	}


  fn short_flag(&self) -> &Option<String> {
    &self.0.short_flag
	}

  fn long_flag(&self) -> &Option<String> {
    &self.0.long_flag
	}

  fn process(&self, app: &mut App) -> ModeResult {
    Err(Error::NotImplementedYet)
  }
}