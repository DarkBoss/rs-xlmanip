use super::super::{Mode, ModeResult, ModeData};
use super::super::super::app::App;
use crate::error::Error;

pub struct ExtractCell(ModeData);

impl ExtractCell {
  pub fn new() -> ExtractCell {
    ExtractCell(ModeData {
      name: "extract-cell".to_string(),
			about: "".to_string(),
      short_flag: Some("xc".to_string()),
      long_flag: Some("extract-cell".to_string()),
    })
  }
}

impl Mode for ExtractCell {
  fn name(&self) -> &String {
    &self.0.name
  }
  
  fn about(&self) -> &String {
    &self.0.name
	}

  fn short_flag(&self) -> &Option<String> {
    &self.0.short_flag
	}

  fn long_flag(&self) -> &Option<String> {
    &self.0.long_flag
	}

  fn process(&self, app: &mut App) -> ModeResult {
    Err(Error::NotImplementedYet)
  }
}