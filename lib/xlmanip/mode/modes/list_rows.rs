use crate::config::Config;
use super::super::super::app::App;
use super::super::*;
use crate::book::*;
use crate::error::Error;
use crate::range::*;
use crate::result::Result;
use calamine::Reader;
use std::collections::HashMap;
pub struct ListRows(ModeData);

impl ListRows {
  pub fn new() -> ListRows {
    ListRows(ModeData {
      name: "list-rows".to_string(),
      about: "List rows of loaded workbooks".to_string(),
      short_flag: Some("lr".to_string()),
      long_flag: Some("list-rows".to_string()),
    })
  }
}

impl ListRows {
  fn process_sheet(&self, book: &mut Book, sheet: &String) -> Result<Range> {
    use std::ops::DerefMut;
    let mut row: Row;
    let mut range: &mut Range;
    match book.get_sheet_mut(sheet) {
      Some(r) => range = r,
      None => return Err(Error::Unknown(format!("{}: unknown sheet", sheet))),
    }
    let cfg = Config::get();
    if let Some(f) = cfg.filter {
      *range = range.filter(f)?;
    }
    let mut message = format!(
      "kept {} rows ({}x{}):\n",
      range.rows().len(),
      range.size().width,
      range.size().height
    );
    for row in range.rows() {
      for cell in row.cells() {
        message = format!("{}\t{}", message, cell.try_lock_n(5)?.value_str());
      }
      message = format!("{}\n", message);
    }
    log::info!("{}", message);
    return Ok(range.clone());
  }

  fn process_book(
    &self,
    book: &mut Book,
    include_sheets: &Vec<String>,
  ) -> Result<()> {
    let sheet_names: Vec<String> = book
      .sheet_names()
      .to_vec()
      .iter()
      .map(|n| n.to_string())
      .collect::<Vec<String>>();
    for sheet in sheet_names {
      if include_sheets.is_empty() || include_sheets.contains(&sheet) {
        self.process_sheet(book, &sheet)?;
      }
    }
    Ok(())
  }
}

impl Mode for ListRows {
  fn name(&self) -> &String {
    &self.0.name
  }
  fn about(&self) -> &String {
    &self.0.name
  }

  fn short_flag(&self) -> &Option<String> {
    &self.0.short_flag
  }

  fn long_flag(&self) -> &Option<String> {
    &self.0.long_flag
  }

  fn process(&self, app: &mut App) -> ModeResult {
    use crate::config::Config;
    let cfg = Config::get();
    use std::ops::DerefMut;
    app
      .books_mut()
      .iter_mut()
      .map(|(_, ref mut b)| self.process_book(*b, &cfg.sheets))
      .collect::<Result<()>>()?;
    Ok(())
  }
}
