use super::super::{Mode, ModeResult, ModeData};
use super::super::super::app::App;
use crate::error::Error;

pub struct UpdateColumn(ModeData);

impl UpdateColumn {
  pub fn new() -> UpdateColumn {
    UpdateColumn(ModeData {
      name: "update-column".to_string(),
			about: "".to_string(),
      short_flag: None,
      long_flag: Some("update-column".to_string()),
    })
  }
}

impl Mode for UpdateColumn {
  fn name(&self) -> &String {
    &self.0.name
	}
  
  fn about(&self) -> &String {
    &self.0.name
	}


  fn short_flag(&self) -> &Option<String> {
    &self.0.short_flag
	}

  fn long_flag(&self) -> &Option<String> {
    &self.0.long_flag
	}

  fn process(&self, app: &mut App) -> ModeResult {
    Err(Error::NotImplementedYet)
  }
}