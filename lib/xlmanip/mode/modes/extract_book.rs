use super::super::{Mode, ModeResult, ModeData};
use super::super::super::app::App;
use crate::error::Error;

pub struct ExtractBook(ModeData);

impl ExtractBook {
  pub fn new() -> ExtractBook {
    ExtractBook(ModeData {
      name: "extract-book".to_string(),
			about: "".to_string(),
      short_flag: Some("xb".to_string()),
      long_flag: Some("extract-book".to_string()),
    })
  }
}

impl Mode for ExtractBook {
  fn name(&self) -> &String {
    &self.0.name
  }
  
  fn about(&self) -> &String {
    &self.0.name
	}

  fn short_flag(&self) -> &Option<String> {
    &self.0.short_flag
	}

  fn long_flag(&self) -> &Option<String> {
    &self.0.long_flag
	}

  fn process(&self, app: &mut App) -> ModeResult {
    Err(Error::NotImplementedYet)
  }
}