use crate::mode::{Mode, ModeResult, ModeData};
use crate::app::App;
use crate::error::Error;
use crate::config::{Config, AppInput};

pub struct ListBooks(ModeData);

impl ListBooks {
  pub fn new() -> ListBooks {
    ListBooks(ModeData {
      name: "list-books".to_string(),
			about: "List books given as input".to_string(),
      short_flag: Some("lb".to_string()),
      long_flag: Some("list-books".to_string()),
    })
  }
}

impl Mode for ListBooks {
  fn name(&self) -> &String {
    &self.0.name
	}
  
  fn about(&self) -> &String {
    &self.0.name
	}


  fn short_flag(&self) -> &Option<String> {
    &self.0.short_flag
	}

  fn long_flag(&self) -> &Option<String> {
    &self.0.long_flag
	}

  fn process(&self, app: &mut App) -> ModeResult {
    for input in Config::get().inputs {
      println!("{}", match input {
        AppInput::File(f) => f,
        AppInput::Stdin => "stdin".to_string(),
      });
    }
    Err(Error::NotImplementedYet)
  }
}