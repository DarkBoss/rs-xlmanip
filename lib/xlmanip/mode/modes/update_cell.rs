use super::super::{Mode, ModeResult, ModeData};
use super::super::super::app::App;
use crate::error::Error;

pub struct UpdateCell(ModeData);

impl UpdateCell {
  pub fn new() -> UpdateCell {
    UpdateCell(ModeData {
      name: "update-cell".to_string(),
			about: "".to_string(),
      short_flag: Some("uc".to_string()),
      long_flag: Some("update-cell".to_string()),
    })
  }
}

impl Mode for UpdateCell {
  fn name(&self) -> &String {
    &self.0.name
	}
  
  fn about(&self) -> &String {
    &self.0.name
	}


  fn short_flag(&self) -> &Option<String> {
    &self.0.short_flag
	}

  fn long_flag(&self) -> &Option<String> {
    &self.0.long_flag
	}

  fn process(&self, app: &mut App) -> ModeResult {
    Err(Error::NotImplementedYet)
  }
}