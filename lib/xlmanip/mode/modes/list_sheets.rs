use super::super::super::{app::App, config::*};
use crate::result::Result;
use super::super::*;
use crate::range::Range;
use crate::book::Book;
use crate::error::Error;

pub struct ListSheets(ModeData);

impl ListSheets {
  pub fn new() -> ListSheets {
    ListSheets(ModeData {
      name: "list-sheets".to_string(),
      about: "List sheets of opened books".to_string(),
      short_flag: Some("ls".to_string()),
      long_flag: Some("list-sheets".to_string()),
    })
  }
}

impl Mode for ListSheets {
  fn name(&self) -> &String {
    &self.0.name
  }
  fn about(&self) -> &String {
    &self.0.name
  }

  fn short_flag(&self) -> &Option<String> {
    &self.0.short_flag
  }

  fn long_flag(&self) -> &Option<String> {
    &self.0.long_flag
  }

  fn process(&self, app: &mut App) -> ModeResult {
    use calamine::Reader;
    for (book_name, book) in app.books() {
      println!("book: {}", book_name);
      for ref sheet in book.sheet_names() {
        println!("\tsheet: {}", sheet);
      }
    }
    Ok(())
  }
}

impl ListSheets {
  fn process_sheet(&self, sheet_name: &String, book: &Book) -> Result<Range> {
    let range = Range::new();
    Ok(range)
  }
}
// for input in &cfg.inputs {
//   match input {
//     AppInput::File(f) => {
//       // opens a new workbook
//       let mut workbook: Xlsx<_> = open_workbook(f.as_str()).expect("Cannot open file");

//       // Read whole worksheet data and provide some statistics
//       if let Some(Ok(range)) = workbook.worksheet_range("Sheet1") {
//         for cell in range.used_cells() {
//           info!("found cell '{:?}'", cell)
//         }
//         let total_cells = range.get_size().0 * range.get_size().1;
//         let non_empty_cells: usize = range.used_cells().count();
//         println!(
//           "Found {} cells in 'Sheet1', including {} non empty cells",
//           total_cells, non_empty_cells
//         );
//         // alternatively, we can manually filter rows
//         assert_eq!(
//           non_empty_cells,
//           range
//             .rows()
//             .flat_map(|r| r.iter().filter(|&c| c != &DataType::Empty))
//             .count()
//         );
//       }

//       // Check if the workbook has a vba project
//       if let Some(Ok(mut vba)) = workbook.vba_project() {
//         let vba = vba.to_mut();
//         let module1 = vba.get_module("Module 1").unwrap();
//         println!("Module 1 code:");
//         println!("{}", module1);
//         for r in vba.get_references() {
//           if r.is_missing() {
//             println!("Reference {} is broken or not accessible", r.name);
//           }
//         }
//       }

//       // You can also get defined names definition (string representation only)
//       for name in workbook.defined_names() {
//         println!("name: {}, formula: {}", name.0, name.1);
//       }

//       // Now get all formula!
//       let sheets = workbook.sheet_names().to_owned();
//       for s in sheets {
//         println!(
//           "found {} formulas in '{}'",
//           workbook
//             .worksheet_formula(&s)
//             .expect("sheet not found")
//             .expect("error while getting formula")
//             .rows()
//             .flat_map(|r| r.iter().filter(|f| !f.is_empty()))
//             .count(),
//           s
//         );
//       }
//     }
//     _ => return Err(Error::NotImplementedYet),
//   };
// }
// Err(Error::NotImplementedYet)
