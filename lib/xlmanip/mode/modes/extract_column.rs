use super::super::{Mode, ModeResult, ModeData};
use super::super::super::app::App;
use crate::error::Error;

pub struct ExtractColumn(ModeData);

impl ExtractColumn {
  pub fn new() -> ExtractColumn {
    ExtractColumn(ModeData {
      name: "extract-column".to_string(),
			about: "".to_string(),
      short_flag: None,
      long_flag: Some("extract-column".to_string()),
    })
  }
}

impl Mode for ExtractColumn {
  fn name(&self) -> &String {
    &self.0.name
	}
  
  fn about(&self) -> &String {
    &self.0.name
	}


  fn short_flag(&self) -> &Option<String> {
    &self.0.short_flag
	}

  fn long_flag(&self) -> &Option<String> {
    &self.0.long_flag
	}

  fn process(&self, app: &mut App) -> ModeResult {
    Err(Error::NotImplementedYet)
  }
}