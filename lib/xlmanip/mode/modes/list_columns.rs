use super::super::super::app::App;
use crate::error::Error;
use super::super::*;
use crate::book::*;
use crate::range::Range;
use crate::result::Result;

use calamine::{Reader, Sheets};
use std::sync::Arc;

pub struct ListColumns(ModeData);

impl ListColumns {
  pub fn new() -> ListColumns {
    ListColumns(ModeData {
      name: "list-columns".to_string(),
      about: "List columns from sheets of opened books".to_string(),
      short_flag: None,
      long_flag: Some("list-columns".to_string()),
    })
  }
}

impl ListColumns {
  fn process_sheet(&self, app: &mut App, book: &Book, sheet: &String) -> Result<Range> {
    use std::ops::DerefMut;
    let mut range = Range::new();
    let mut line = String::new();
    Ok(range)
  }

  fn process_book(
    &self, 
    app: &mut App,
    book_name: &String,
    book: &mut Book,
    include_sheets: &Vec<String>,
  ) -> Result<()> {
    let sheet_names: Vec<&String> = book.sheet_names().to_vec();
    println!("* {}", book_name);
    for sheet in sheet_names {
      if include_sheets.is_empty() || include_sheets.contains(&sheet) {
        println!("  [{}]", &sheet);
      }
    }
    Ok(())
  }
}

impl Mode for ListColumns {
  fn name(&self) -> &String {
    &self.0.name
  }
  fn about(&self) -> &String {
    &self.0.name
  }

  fn short_flag(&self) -> &Option<String> {
    &self.0.short_flag
  }

  fn long_flag(&self) -> &Option<String> {
    &self.0.long_flag
  }

  fn process(&self, app: &mut App) -> ModeResult {
    use crate::config::Config;
    let cfg = Config::get();
    let mut books = app.books().clone();
    for (ref book_name, ref mut book) in books {
      self.process_book(app, book_name, book, &cfg.sheets)?;
    }
    Ok(())
  }
}
