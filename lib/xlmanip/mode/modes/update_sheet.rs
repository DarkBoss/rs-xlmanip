use super::super::{Mode, ModeResult, ModeData};
use super::super::super::app::App;
use crate::error::Error;

pub struct UpdateSheet(ModeData);

impl UpdateSheet {
  pub fn new() -> UpdateSheet {
    UpdateSheet(ModeData {
      name: "update-sheet".to_string(),
			about: "".to_string(),
      short_flag: Some("us".to_string()),
      long_flag: Some("update-sheet".to_string()),
    })
  }
}

impl Mode for UpdateSheet {
  fn name(&self) -> &String {
    &self.0.name
	}
  
  fn about(&self) -> &String {
    &self.0.name
	}


  fn short_flag(&self) -> &Option<String> {
    &self.0.short_flag
	}

  fn long_flag(&self) -> &Option<String> {
    &self.0.long_flag
	}

  fn process(&self, app: &mut App) -> ModeResult {
    Err(Error::NotImplementedYet)
  }
}