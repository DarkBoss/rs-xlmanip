#[macro_use]
use crate::mode::result::ModeResult;
use crate::range::Range;
use super::mode::*;
use crate::app::App;
use crate::error::Error;
use crate::result::Result;

mode!(
  list_books,
  list_columns,
  list_sheets,
  list_rows
);

mode!(
  extract_book,
  extract_cell,
  extract_column,
  extract_row,
  extract_sheet
);

mode!(update_cell, update_column, update_row, update_sheet);

use std::sync::Arc;

static mut g_modes: Option<Vec<Arc<dyn Mode>>> = None;

pub fn all_modes() -> Vec<Arc<dyn Mode>> {
  unsafe {
    if g_modes.is_none() {
      g_modes = Some(vec![
        Arc::new(ListBooks::new()),
        Arc::new(ListColumns::new()),
        Arc::new(ListSheets::new()),
        Arc::new(ListRows::new()),
        Arc::new(ExtractBook::new()),
        Arc::new(ExtractCell::new()),
        Arc::new(ExtractColumn::new()),
        Arc::new(ExtractRow::new()),
        Arc::new(ExtractSheet::new()),
        Arc::new(UpdateCell::new()),
        Arc::new(UpdateColumn::new()),
        Arc::new(UpdateRow::new()),
        Arc::new(UpdateSheet::new()),
      ]);
    }
    g_modes.clone().unwrap().clone()
  }
}

pub fn get_mode<S: AsRef<str>>(n: S) -> Option<Arc<dyn Mode>> {
  if let Some(m) = all_modes().iter().find(|m| *m.name() == n.as_ref()) {
    return Some(m.clone());
  }
  None
}

pub fn exec_mode<S: AsRef<str>>(n: S, app: &mut App) -> ModeResult {
  let n = n.as_ref().to_string();
  match get_mode(&n) {
    None => return Err(Error::Unknown(format!("unknown app mode '{}'", &n))),
    Some(m) => return m.process(app),
  }
}
