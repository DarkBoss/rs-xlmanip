use crate::fmt::WriterMap;
use crate::range::Row;
use crate::fmt::Writer;
use crate::book::{Book, BookStore};
use crate::config::Config as AppConfig;
use crate::fmt::{Config as SerializerConfig, Serializer};
use crate::range::Range;
use std::io::Write;
use std::collections::HashMap;

/**
 * Memory buffer with read/write capabilities
 */
pub struct StrBuf {
  data: std::rc::Rc<std::cell::RefCell<String>>
}

impl std::string::ToString for StrBuf {
  fn to_string(&self) -> String {
    String::from(self.data.borrow().clone())
  }
}

impl StrBuf {
  pub fn new(b: std::rc::Rc<std::cell::RefCell<String>>) -> StrBuf {
    StrBuf {
      data: b,
    }
  }

  pub fn data(&self) -> &std::rc::Rc<std::cell::RefCell<String>> {
    &self.data
  }

  pub fn data_mut(&mut self) -> &mut std::rc::Rc<std::cell::RefCell<String>> {
    &mut self.data
  }
}

impl std::io::Write for StrBuf {
  fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
    let part = std::str::from_utf8(buf).expect("failed to convert utf8 string");
    use std::ops::Deref;
    use std::ops::DerefMut;
    let tmp: String = format!("{}{}", self.data.borrow().deref(), part);
    *self.data.borrow_mut().deref_mut() = tmp.clone();
    Ok(part.len())
  }

  fn flush(&mut self) -> std::io::Result<()> {
    Ok(())
  }
}

pub fn create_store(book_name: &str, sheet_name: &str, rows: Vec<Vec<&str>>) -> BookStore {
  let mut book = Book::with_path(book_name.to_string());
  book.sheets_mut().insert(
    sheet_name.to_string(),
    Range::from(
      rows
        .iter()
        .map(|row| row.iter().map(|v| v.to_string()).collect::<Vec<String>>())
        .collect::<Vec<Vec<String>>>(),
    ),
  );
  BookStore::from(vec![book])
}

pub fn create_config() -> (AppConfig, SerializerConfig) {
  let ser_cfg = SerializerConfig::default();
  let mut app_cfg = AppConfig::default();
  app_cfg.force = true;
  (app_cfg, ser_cfg)
}

pub fn serialize<S: Serializer>(mut ser: S, store: &mut BookStore) -> String {
  let (app_cfg, _ser_cfg) = create_config();
  ser
    .configure(&store, &app_cfg, &SerializerConfig::default())
    .expect("failed to configure serializer");
  use std::cell::RefCell;
  use std::rc::Rc;
  let buf = Rc::new(RefCell::new(String::new()));
  let tw: StrBuf = StrBuf::new(buf.clone());
  ser.app_config_mut().output = crate::config::AppOutput::Custom(std::rc::Rc::new(std::cell::RefCell::new(tw)));
  let mut book = store.get_mut(&String::from("test.xlsx")).unwrap();
  ser.serialize(&mut book)
    .expect("failed to serialize");
  buf.clone().borrow().clone()
}
