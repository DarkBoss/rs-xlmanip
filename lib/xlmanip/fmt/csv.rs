use super::{Config as SerializerConfig, Serializer};
use crate::book::Book;
use crate::book::BookStore;
use crate::config::{AppOutput, Config as AppConfig};
use crate::error::Error;
use crate::fmt::Writer;
use crate::fmt::WriterMap;
use crate::cell::Range;
use crate::cell::Row;
use crate::result::Result;
use bytes::BufMut;
use std::collections::HashMap;
use std::ffi::OsStr;

const TAB: &'static str = "\t";
const NEW_LINE: &'static str = "\n";
const COMMA: &'static str = ",";
const SEMI_COLON: &'static str = ";";

const TSV_NAME: &'static str = "Comma separated value";
const TSV_EXT: &'static str = "csv";
const TSV_DESC: &'static str = "Print results as tab separated values";
const TSV_CELL_DELIM: &'static str = TAB;
const TSV_ROW_DELIM: &'static str = NEW_LINE;

const CSV_NAME: &'static str = "CSV";
const CSV_EXT: &'static str = "csv";
const CSV_DESC: &'static str = "Print results as comma separated values";
const CSV_CELL_DELIM: &'static str = COMMA;
const CSV_ROW_DELIM: &'static str = NEW_LINE;

const SCSV_NAME: &'static str = "SCSV";
const SCSV_EXT: &'static str = "csv";
const SCSV_DESC: &'static str = "Print results as semi-colon separated values";
const SCSV_CELL_DELIM: &'static str = SEMI_COLON;
const SCSV_ROW_DELIM: &'static str = NEW_LINE;

pub struct CSVTypeData {
  name: String,
  ext: String,
  desc: String,
  cell: String,
  row: String,
}

pub enum CSVKind {
  TSV,
  CSV,
  SCSV,
}

impl CSVKind {
  pub fn data(&self) -> CSVTypeData {
    match self {
      CSVKind::TSV => CSVTypeData {
        name: TSV_NAME.to_string(),
        ext: TSV_EXT.to_string(),
        desc: TSV_DESC.to_string(),
        cell: String::from(TSV_CELL_DELIM),
        row: String::from(TSV_ROW_DELIM),
      },
      CSVKind::CSV => CSVTypeData {
        name: CSV_NAME.to_string(),
        ext: CSV_EXT.to_string(),
        desc: CSV_DESC.to_string(),
        cell: String::from(CSV_CELL_DELIM),
        row: String::from(CSV_ROW_DELIM),
      },
      CSVKind::SCSV => CSVTypeData {
        name: SCSV_NAME.to_string(),
        ext: SCSV_EXT.to_string(),
        desc: SCSV_DESC.to_string(),
        cell: String::from(SCSV_CELL_DELIM),
        row: String::from(SCSV_ROW_DELIM),
      },
    }
  }

  pub fn cell_delim(&self) -> String {
    self.data().cell
  }

  pub fn row_delim(&self) -> String {
    self.data().row
  }
  pub fn name(&self) -> String {
    self.data().name
  }

  pub fn extension(&self) -> String {
    self.data().ext
  }
  pub fn description(&self) -> String {
    self.data().desc
  }
}

pub struct CSV {
  kind: CSVKind,
  ser_cfg: SerializerConfig,
  app_cfg: AppConfig,
}

impl Serializer for CSV {
  fn extension(&self) -> String {
    self.kind.extension()
  }
  fn name(&self) -> String {
    self.kind.name()
  }

  fn app_config(&self) -> &AppConfig {
    &self.app_cfg
  }
  fn app_config_mut(&mut self) -> &mut AppConfig {
    &mut self.app_cfg
  }

  fn config(&self) -> &SerializerConfig {
    &self.ser_cfg
  }
  fn config_mut(&mut self) -> &mut SerializerConfig {
    &mut self.ser_cfg
  }

  fn description(&self) -> String {
    self.kind.description()
  }

  fn configure(
    &mut self,
    book_store: &BookStore,
    ac: &AppConfig,
    sc: &SerializerConfig,
  ) -> Result<()> {
    self.app_cfg = ac.clone();
    self.ser_cfg = sc.clone();
    Ok(())
  }

  fn serialize(&mut self, book: &Book) -> Result<()> {
    use std::collections::HashMap;
    let mut ret: String;
    let mut contents: HashMap<String, String> = HashMap::new();
    for (name, sheet) in book.sheets() {
      ret = String::new();
      if self.ser_cfg.write_header {
        ret = format!("{}{}", self.fmt_header(sheet)?, self.kind.row_delim());
      }
      if self.ser_cfg.write_rows {
        ret = format!("{}{}{}", ret, self.fmt_rows(sheet)?, self.kind.row_delim());
      }
      let fname = self.prepare_sheet_key(book.name(), sheet.sheet_name());
      let mut w = self.create_writer(book.name(), sheet.sheet_name())?;
      w.borrow_mut().write(ret.as_bytes()).or_else(|e| Err(Error::IO(e)))?;
    }
    Ok(())
  }
}

impl CSV {
  pub fn new(t: CSVKind) -> CSV {
    CSV {
      kind: t,
      app_cfg: AppConfig::default(),
      ser_cfg: SerializerConfig::default(),
    }
  }

  pub fn new_tsv() -> CSV {
    CSV::new(CSVKind::TSV)
  }

  pub fn new_csv() -> CSV {
    CSV::new(CSVKind::CSV)
  }

  pub fn new_scsv() -> CSV {
    CSV::new(CSVKind::SCSV)
  }

  pub fn fmt_header(&self, range: &Range) -> Result<String> {
    let mut header = format!("{}", range.header().join(&self.kind.cell_delim()));
    Ok(header)
  }

  pub fn fmt_rows(&self, range: &Range) -> Result<String> {
    use std::io::Write;
    Ok(
      range
        .rows()
        .iter()
        .enumerate()
        .map(|(i, ref r)| self.fmt_row(&r))
        .collect::<Result<Vec<String>>>()?
        .join(&self.kind.row_delim()),
    )
  }

  pub fn fmt_row(&self, r: &Row) -> Result<String> {
    Ok(format!("{}", r.join(&self.kind.cell_delim())))
  }

  fn create_writer(&mut self, book_name: &String, sheet_name: &String) -> Result<Writer> {
    let key = self.prepare_sheet_key(book_name, sheet_name);
    let os_to_string = |s: Option<&OsStr>| match s {
      Some(n) => match n.to_str() {
        Some(n) => n.to_string(),
        None => String::new(),
      },
      None => String::new(),
    };
    if let AppOutput::File(ref f) = &self.app_cfg.output {
      // inject sheet and book names
      let mut path = f.to_string();
      let p = std::path::Path::new(&path);
      let stem = os_to_string(p.file_stem());
      let sheet = self.prepare_sheet_filename(book_name, sheet_name, &os_to_string(p.extension()));
      path = match std::path::Path::new(&stem).join(sheet).to_str() {
        Some(p) => p.to_string(),
        None => f.to_string(),
      };
      if std::path::Path::new(&path).exists() {
        if self.app_cfg.force {
          std::fs::remove_file(&path).or_else(|e| Err(Error::IO(e)))?;
        } else {
          return Err(Error::Unknown(format!("{}: file already exists!", &path)));
        }
      }
      self.app_cfg.output = AppOutput::File(path);
    }
    log::debug!(
      "writer for {}::{} is '{}' -> {}",
      book_name,
      sheet_name,
      self.app_cfg.output.kind(),
      self.app_cfg.output.url()
    );
    self.app_cfg.output.create()
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use super::super::tests::*;

  #[test]
  fn tsv_serializer_should_work() {
    let ser = CSV::new(CSVKind::TSV);
    let header = vec!["col1", "col2", "col3"];
    let row = vec!["val1", "val2", "val3"];
    let rows = vec![header, row];
    let mut store = create_store("test.xlsx", "sheet1", rows);
    assert_eq!(serialize(ser, &mut store), "col1\tcol2\tcol3\nval1\tval2\tval3\n")
  }

  #[test]
  fn csv_serializer_should_work() {
    let ser = CSV::new(CSVKind::CSV);
    let header = vec!["col1", "col2", "col3"];
    let row = vec!["val1", "val2", "val3"];
    let rows = vec![header, row];
    let mut store = create_store("test.xlsx", "sheet1", rows);

    assert_eq!(serialize(ser, &mut store), "col1,col2,col3\nval1,val2,val3\n")
  }

  #[test]
  fn scsv_serializer_should_work() {
    let ser = CSV::new(CSVKind::SCSV);
    let header = vec!["col1", "col2", "col3"];
    let row = vec!["val1", "val2", "val3"];
    let rows = vec![header, row];
    let mut store = create_store("test.xlsx", "sheet1", rows);
    assert_eq!(serialize(ser, &mut store), "col1;col2;col3\nval1;val2;val3\n")
  }
}
