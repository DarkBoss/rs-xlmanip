use crate::fmt::Writer;
use crate::config::AppOutput;
use crate::book::BookStore;
use std::collections::HashMap;
use crate::fmt::WriterMap;
use crate::book::Book;
use crate::result::Result;
use crate::config::Config as AppConfig;
use crate::error::Error;
use crate::range::Range;
use super::{Serializer, Config};

pub struct Debug {
  ser_cfg: Config,
  app_cfg: AppConfig
}

const DEBUG_NAME: &'static str = "Debug";
const DEBUG_EXT: &'static str = ".txt";
const DEBUG_DESC: &'static str = "Pretty print results";

impl Debug {
  pub fn new() -> Debug {
    Debug{
      app_cfg: AppConfig::default(),
      ser_cfg: Config::default()
    }
  }

}

use std::io::Write;

impl Serializer for Debug {
  fn extension(&self) -> String {
    String::from(DEBUG_EXT)
  }
  fn name(&self) -> String {
    String::from(DEBUG_NAME)
  }
  fn description(&self) -> String {
    String::from(DEBUG_DESC)
  }

  fn config(&self) -> &Config {
    &self.ser_cfg
  }

  fn config_mut(&mut self) -> &mut Config {
    &mut self.ser_cfg
  }

  fn configure(&mut self, books: &BookStore, ac: &AppConfig, sc: &Config) -> Result<()> {
    self.app_cfg = ac.clone();
    self.ser_cfg = sc.clone();
    Ok(())
  }

  fn app_config(&self) -> &AppConfig {
    &self.app_cfg
  }
  fn app_config_mut(&mut self) -> &mut AppConfig {
    &mut self.app_cfg
  }
  
  fn serialize(&mut self, b: &Book) -> Result<()> {
    use std::io::Write;
    use std::ops::DerefMut;
    for (sheet_name, sheet) in b.sheets() {
      let mut w = self.create_writer(b.name(), sheet_name)?;
      w.borrow_mut().write(format!("{:?}", b).as_bytes()).or_else(|e| Err(Error::IO(e)))?;
    }
    Ok(())
  }
}

#[cfg(test)]
mod tests {
  use crate::fmt::Writer;
  use crate::fmt::tests::*;
  use super::*;
  
  #[test]
  fn debug_serializer_should_output_something() {
    let mut ser = Debug::new();
    let mut acfg = AppConfig::default();
    let mut scfg = Config::default();
    let mut store = create_store("book", "sheet", vec![
      vec![
        "a", "b", "c"
      ]
    ]);
    use std::cell::RefCell;
    use std::rc::Rc;
    let mut buf = Rc::new(RefCell::new(String::new()));
    let mut tw: StrBuf = StrBuf::new(buf.clone());
    acfg.output = crate::config::AppOutput::Custom(std::rc::Rc::new(std::cell::RefCell::new(tw)));
    ser.configure(&mut store, &acfg, &scfg).expect("failed to configure");

    use std::iter::FromIterator;
    use std::io::{Cursor, Read, Seek, SeekFrom, Write};

    unsafe {
      let book = store.get(&"book".to_string()).unwrap();
      ser.serialize(&book).expect("failed to serialize");
      assert_eq!(
        format!("{:?}", book),
        buf.clone().borrow().clone()
      );
    }
  }
}