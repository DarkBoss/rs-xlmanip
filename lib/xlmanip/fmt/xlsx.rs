use crate::book::Book;
use crate::book::BookStore;
use crate::cell::cell::Cell;
use crate::error::Error;
use crate::fmt::Serializer;
use crate::result::Result;
use crate::config::AppOutput;
use xlsxwriter as xlw;

use crate::config::Config as AppConfig;
use crate::fmt::Config as SerializerConfig;

pub struct Xlsx {
  ser_cfg: SerializerConfig,
  app_cfg: AppConfig,
}

impl Xlsx {
  pub fn new() -> Xlsx {
    Xlsx {
      ser_cfg: SerializerConfig::default(),
      app_cfg: AppConfig::default(),
    }
  }
}

impl Serializer for Xlsx {
  fn extension(&self) -> String {
    ".xlsx".to_string()
  }
  fn name(&self) -> String {
    "xlsx".to_string()
  }

  fn app_config(&self) -> &AppConfig {
    &self.app_cfg
  }
  fn app_config_mut(&mut self) -> &mut AppConfig {
    &mut self.app_cfg
  }

  fn config(&self) -> &SerializerConfig {
    &self.ser_cfg
  }
  fn config_mut(&mut self) -> &mut SerializerConfig {
    &mut self.ser_cfg
  }

  fn description(&self) -> String {
    "OpenOffice Xml Spreadsheet".to_string()
  }

  fn configure(
    &mut self,
    book_store: &BookStore,
    ac: &AppConfig,
    sc: &SerializerConfig,
  ) -> Result<()> {
    self.app_cfg = ac.clone();
    self.ser_cfg = sc.clone();
    Ok(())
  }

  fn serialize(&mut self, book: &Book) -> Result<()> {
    use std::collections::HashMap;
    let mut ret: String;
    let mut contents: HashMap<String, String> = HashMap::new();
    let out_dir = match &self.app_cfg.output {
      AppOutput::File(path) => path.clone(),
      _ => return Err(Error::Unknown(format!("cannot write to stdout")))
    };
    let mut out_name = book.name().clone();
    if !out_name.ends_with(self.extension().as_str()) {
      out_name = format!("{}{}", out_name, self.extension());
    }
    let filename = std::path::Path::new(out_dir.as_str()).join(out_name.as_str());
    let sfilename = filename.into_os_string().into_string().expect("failed to convert path to string");
    let workbook = xlw::Workbook::new(sfilename.as_str());

    for (name, sheet) in book.sheets() {
      let mut sheet1 = workbook.add_worksheet(Some(name)).or_else(|e| {
        Err(Error::Unknown(format!(
          "failed to add sheet '{}', {}",
          name,
          e.to_string()
        )))
      })?;
      for cell in sheet.header().cells() {
        self
          .write_value(cell, &mut sheet1)
          .or_else(|e| Err(Error::Unknown(e.to_string())))?;
      }
      for row in sheet.rows() {
        for cell in row.cells() {
          self
            .write_value(cell, &mut sheet1)
            .or_else(|e| Err(Error::Unknown(e.to_string())))?;
        }
      }
    }

    workbook.close().or_else(|e| {
      Err(Error::Unknown(format!(
        "failed to close workbook '{}', {}",
        book.name(),
        e.to_string()
      )))
    })?;
    Ok(())
  }
}

impl Xlsx {
  fn write_value<'a>(&self, cell: &Cell, sh: &mut xlw::Worksheet<'a>) -> Result<()> {
    let local_cell = cell
      .try_lock_n(5)
      .or_else(|e| Err(Error::Unknown(e.to_string())))?;
    let val = match local_cell.value() {
      Some(v) => format!("{}", v),
      None => String::new(),
    };
    log::info!(
      "Writing '{}' to {}x{}",
      val.clone(),
      local_cell.address().x as u16,
      local_cell.address().y as u32,
    );
    sh.write_string(
      local_cell.address().y as u32,
      local_cell.address().x as u16,
      val.as_ref(),
      None,
    )
    .or_else(|e| {
      Err(Error::Unknown(format!(
        "failed to write string, {}",
        e.to_string()
      )))
    })?;
    Ok(())
  }
}
