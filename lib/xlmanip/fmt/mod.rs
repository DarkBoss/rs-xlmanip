pub mod csv;
pub mod debug;
pub mod xlsx;

pub mod tests;

use crate::error::Error;
use crate::book::BookStore;
use crate::book::Book;
use super::result::Result;
use super::range::Range;
use crate::config::{Config as AppConfig, AppOutput};

use std::io::Write;
use std::collections::HashMap;

#[derive(Clone)]
pub struct Config {
  pub write_header: bool,
  pub write_rows: bool,
}

impl std::default::Default for Config {
  fn default() -> Config {
    Config {
      write_header: true,
      write_rows: true,
    }
  }
}

pub type Writer<'a> = std::rc::Rc<std::cell::RefCell<dyn std::io::Write + 'a>>;
pub type WriterMap<'a> = std::collections::HashMap<String, Writer<'a>>;

pub trait Serializer {
  fn extension(&self) -> String;
  fn name(&self) -> String;
  fn description(&self) -> String;

  fn app_config(&self) -> &AppConfig;
  fn app_config_mut(&mut self) -> &mut AppConfig;

  fn config(&self) -> &Config;
  fn config_mut(&mut self) -> &mut Config;

  fn prepare_sheet_filename(
    &self,
    book_name: &String,
    sheet_name: &String,
    ext: &String,
  ) -> String {
    let book_name_wo_ext = match std::path::Path::new(book_name).file_stem() {
      Some(n) => match n.to_str() {
        Some(n) => n.to_string(),
        None => book_name.clone(),
      },
      None => book_name.clone(),
    };
    let local_ext = match ext.is_empty() {
      true => self.extension().clone(),
      false => ext.clone(),
    };
    format!("{}_{}.{}", book_name_wo_ext, sheet_name, local_ext)
  }

  fn prepare_sheet_key(&self, book_name: &String, sheet_name: &String) -> String {
    format!("{}::{}", book_name, sheet_name)
  }
  
  fn create_writer(&mut self, book_name: &String, sheet_name: &String) -> Result<Writer> {
    let key = self.prepare_sheet_key(book_name, sheet_name);
    let os_to_string = |s: Option<&std::ffi::OsStr>| match s {
      Some(n) => match n.to_str() {
        Some(n) => n.to_string(),
        None => String::new(),
      },
      None => String::new(),
    };
    let app_cfg = self.app_config().clone();
    let ser_cfg = self.config().clone();
    if let AppOutput::File(ref f) = &app_cfg.output {
      // inject sheet and book names
      let mut path = f.to_string();
      let p = std::path::Path::new(&path);
      let stem = os_to_string(p.file_stem());
      let sheet = self.prepare_sheet_filename(book_name, sheet_name, &os_to_string(p.extension()));
      path = match std::path::Path::new(&stem).join(sheet).to_str() {
        Some(p) => p.to_string(),
        None => f.to_string(),
      };
      if std::path::Path::new(&path).exists() {
        if app_cfg.force {
          std::fs::remove_file(&path).or_else(|e| Err(Error::IO(e)))?;
        } else {
          return Err(Error::Unknown(format!("{}: file already exists!", &path)));
        }
      }
      self.app_config_mut().output = AppOutput::File(path);
    }
    log::debug!(
      "writer for {}::{} is '{}' -> {}",
      book_name,
      sheet_name,
      app_cfg.output.kind(),
      app_cfg.output.url()
    );
    self.app_config().output.create()
  }

  fn configure(&mut self, books: &BookStore, ac: &AppConfig, sc: &Config) -> Result<()>;
  fn serialize(&mut self, b: &Book) -> Result<()>;
}

pub type BoxedSerializer = Box<dyn Serializer>;

pub use csv::*;
pub use debug::*;