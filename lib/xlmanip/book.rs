use crate::error::Error;
use crate::cell::Cell;
use crate::query::expr::*;
use crate::query::filter::Filter;
use crate::query::token::Token;
use crate::cell::{Range, Row, Value};
use crate::result::Result;
use calamine::Reader;
use std::collections::HashMap;

#[derive(Clone, Debug)]
pub struct Book {
  name: String,
  path: String,
  sheets: HashMap<String, Range>,
}

impl Book {
  pub fn new() -> Book {
    Book {
      name: String::new(),
      path: String::new(),
      sheets: HashMap::new()
    }
  }

  pub fn with_path(path: String) -> Book {
    Self::with_values(path, HashMap::new())
  }

  pub fn with_values(path: String, sheets: HashMap<String, Range>) -> Book {
    let basename = match std::path::Path::new(&path).file_name() {
      Some(bn) => match bn.to_str() {
        Some(bn) => bn.to_string(),
        None => path.to_string(),
      }
      None => path.to_string()
    };
    Book {
      name: basename,
      path,
      sheets,
    }
  }

  pub fn load(path: String) -> Result<Book> {
    let mut cbook = calamine::open_workbook_auto(path.to_string()).or_else(|e| {
      Err(Error::Unknown(format!(
        "{}: failed to open workbook",
        &path
      )))
    })?;
    let mut book = Book::with_values(path, HashMap::new());
    book.process(&mut cbook)?;
    Ok(book)
  }

  pub fn name(&self) -> &String {
    &self.name
  }
  pub fn name_mut(&mut self) -> &mut String {
    &mut self.name
  }

  pub fn get_sheet(&self, name: &String) -> Option<&Range> {
    self.sheets.get(name)
  }

  pub fn get_sheet_mut(&mut self, name: &String) -> Option<&mut Range> {
    self.sheets.get_mut(name)
  }

  pub fn sheets(&self) -> &HashMap<String, Range> {
    &self.sheets
  }

  pub fn sheets_mut(&mut self) -> &mut HashMap<String, Range> {
    &mut self.sheets
  }

  fn process_sheet(&mut self, sheets: &mut calamine::Sheets, sheet_name: &String) -> Result<Range> {
    use std::ops::DerefMut;
    let mut row: Row;
    let mut range = Range::new();
    if let Some(cells) = sheets.worksheet_range(sheet_name) {
      match cells.or_else(|e| Err(Error::Unknown(format!("{}", e.to_string())))) {
        Err(e) => return Err(Error::Unknown(e.to_string())),
        Ok(cells) => {
          let sz = cells.get_size();
          for y in 0 as u32..sz.1 as u32 {
            row = Row::new();
            for x in 0 as u32..sz.0 as u32 {
              let val = match cells.get_value((y, x)) {
                Some(v) => format!("{}", v),
                None => "None".to_string(),
              };
              row.push(Cell::from_value(Value::Text(val)));
            }
            if y == 0 {
              range.set_header(row.clone());
            } else {
              range.push_row(row.clone());
            }
            *range.sheet_name_mut() = sheet_name.clone();
          }
        }
      }
    }
    range.update_addresses();
    self.sheets.insert(sheet_name.clone(), range.clone());
    Ok(range)
  }

  fn process(&mut self, sheets: &mut calamine::Sheets) -> Result<()> {
    use crate::config::Config;
    let cfg = Config::get();
    let sheet_names = sheets.sheet_names().to_vec();
    for ref sheet_name in sheet_names {
      self.process_sheet(sheets, sheet_name)?;
    }
    Ok(())
  }

  pub fn sheet_names(&self) -> Vec<&String> {
    self.sheets.keys().collect()
  }
}

pub type BookMap = HashMap<String, Book>;

#[derive(Debug, Clone)]
pub struct BookStore(BookMap);

impl std::ops::Deref for BookStore {
  type Target = BookMap;

  fn deref(&self) -> &Self::Target {
    &self.0
  }
}


impl std::ops::DerefMut for BookStore {
  fn deref_mut(&mut self) -> &mut Self::Target {
    &mut self.0
  }
}

impl std::convert::From<Vec<Book>> for BookStore {
  fn from(books: Vec<Book>) -> Self {
    let mut store = BookStore::new();
    for book in books {
      store.books_mut().insert(book.name().clone(), book);
    }
    store
  }
}
impl BookStore {
  pub fn new() -> BookStore {
    BookStore(HashMap::new())
  }

  pub fn books(&self) -> &BookMap {
    &self.0
  }

  pub fn books_mut(&mut self) -> &mut BookMap {
    &mut self.0
  }

  pub fn get(&mut self, n: &String) -> Option<&Book> {
    self.0.get(n)
  }

  pub fn load(&mut self, n: String) -> Result<Book> {
    let book = Book::load(n.to_string())?;
    log::debug!("read book: {:#?}", book);
    self.0.insert(
      n.to_string(),
      book,
    );
    if let Some(book) = self.get(&n) {
      return Ok(book.clone());
    }
    Err(Error::Unknown(format!(
      "failed to find {:?}",
      n.to_string()
    )))
  }
}
