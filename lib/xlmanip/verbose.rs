#[derive(Debug, Copy, Clone, PartialEq, PartialOrd, Eq)]
pub enum VerboseLevel {
  Silent,
  Discreet,
  Normal,
  Talkative,
}

impl VerboseLevel {
  pub fn value(&self) -> u8 {
    match self {
      VerboseLevel::Silent => 0,
      VerboseLevel::Discreet => 1,
      VerboseLevel::Normal => 2,
      VerboseLevel::Talkative => 3,
    }
  }

  pub fn name(&self) -> &'static str {
    match self {
      VerboseLevel::Silent => "silent",
      VerboseLevel::Discreet => "discreet",
      VerboseLevel::Normal => "normal",
      VerboseLevel::Talkative => "talkative",
    }
  }
}

impl std::fmt::Display for VerboseLevel {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{}", self.name())
  }
}