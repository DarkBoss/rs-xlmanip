use crate::book::Book;
use crate::book::BookMap;
use crate::book::BookStore;
use crate::config::{AppInput, AppOutput, Config};
use crate::error::Error;
use crate::fmt::BoxedSerializer;
use crate::fmt::Config as SerializerConfig;
use crate::fmt::Writer;
use crate::mode::Mode;
use crate::mode::ModeResult;
use crate::result::Result;

use log::*;

use std::collections::HashMap;

use crate::fmt::csv::CSV;
use crate::fmt::xlsx::Xlsx;
use crate::fmt::debug::Debug;

pub type SerializerMap = HashMap<String, BoxedSerializer>;
static mut G_SERIALIZERS: Option<SerializerMap> = None;

fn build_serializers() -> &'static mut HashMap<String, BoxedSerializer> {
  unsafe {
    if G_SERIALIZERS.is_none() {
      let data: Vec<(String, BoxedSerializer)> = vec![
        (String::from("tsv"), Box::new(CSV::new_tsv())),
        (String::from("csv"), Box::new(CSV::new_csv())),
        (String::from("xlsx"), Box::new(Xlsx::new())),
        (String::from("scsv"), Box::new(CSV::new_scsv())),
        (String::from("debug"), Box::new(Debug::new())),
      ];
      use std::iter::FromIterator;
      G_SERIALIZERS = Some(HashMap::from_iter(data));
    }
    return G_SERIALIZERS.as_mut().unwrap();
  }
}

pub fn put_serializer<S: AsRef<str>>(n: S, ser: BoxedSerializer) {
  build_serializers().insert(n.as_ref().to_string(), ser);
}

pub fn get_serializer<S: AsRef<str>>(n: S) -> Option<&'static BoxedSerializer> {
  let sn = n.as_ref().to_string();
  build_serializers();
  unsafe {
    for (ref k, ref v) in G_SERIALIZERS.as_ref().unwrap() {
      if sn == **k || sn == v.name() || sn == v.extension() {
        return Some(v);
      }
    }
    None
  }
}

pub fn get_serializer_mut<S: AsRef<str>>(n: S) -> Option<&'static mut BoxedSerializer> {
  let sn = n.as_ref().to_string();
  build_serializers();
  unsafe {
    for (k, v) in G_SERIALIZERS.as_mut().unwrap() {
      if sn == **k || sn == v.name() || sn == v.extension() {
        return Some(v);
      }
    }
    None
  }
}

pub fn get_serializers() -> &'static HashMap<String, BoxedSerializer> {
  build_serializers();
  unsafe { G_SERIALIZERS.as_ref().unwrap() }
}

pub fn get_serializers_mut() -> &'static mut HashMap<String, BoxedSerializer> {
  return build_serializers();
}

pub fn register_serializer<S: AsRef<str>>(n: S, f: BoxedSerializer) {
  let sn = n.as_ref().to_string();
  let existing = build_serializers().get(&sn);
  if existing.is_some() {
    panic!("serializer already exists '{}'", sn)
  }
  unsafe {
    G_SERIALIZERS.as_mut().unwrap().insert(sn, f);
  }
}

pub struct App {
  books: BookStore,
}

impl App {
  pub fn new() -> App {
    App {
      books: BookStore::new(),
    }
  }

  pub fn config(&self) -> Config {
    Config::get()
  }

  pub fn book_store(&self) -> &BookStore {
    &self.books
  }

  pub fn book_store_mut(&mut self) -> &mut BookStore {
    &mut self.books
  }

  pub fn books(&self) -> &BookMap {
    self.books.books()
  }

  pub fn books_mut(&mut self) -> &mut BookMap {
    self.books.books_mut()
  }

  pub fn run(&mut self) -> Result<()> {
    let cfg = Config::get();
    let mode_name = cfg.mode.unwrap().clone();
    for input in &cfg.inputs {
      match input {
        AppInput::File(f) => self.books.load(f.to_string())?,
        AppInput::Stdin => {
          return Err(Error::Unknown(
            "stdin reading not yet supported".to_string(),
          ))
        }
      };
    }
    crate::mode::exec_mode(&mode_name, self)?;
    self.write_results()
  }

  fn write_results(&self) -> Result<()> {
    let cfg = Config::get();
    match get_serializer_mut(cfg.format.as_ref().unwrap()) {
      None => {
        return Err(Error::Unknown(format!(
          "unknown format '{}'",
          cfg.format.as_ref().unwrap()
        )))
      }
      Some(f) => {
        f.configure(&self.books, &cfg, &SerializerConfig::default())?;
        for (_, ref book) in self.books() {
          f.serialize(book)?;
        }
      }
    }
    Ok(())
  }
}
