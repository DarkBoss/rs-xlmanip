use super::cell::{Cell, CellInner, CellMutex};
use crate::cell::cell::Value;
use crate::coords::{Point, Size};
use crate::error::Error;
use crate::query::filter::Filter;
use crate::query::token::Token;
use crate::result::Result;

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Row(Vec<Cell>);

impl std::convert::From<Vec<Cell>> for Row {
  fn from(data: Vec<Cell>) -> Row {
    Row(data)
  }
}

impl std::convert::From<Vec<String>> for Row {
  fn from(data: Vec<String>) -> Row {
    let mut ret: Vec<Cell> = vec![];
    let mut r: CellInner;
    for item in data {
      ret.push(Cell::new(None));
    }
    Row(ret)
  }
}

impl Row {
  pub fn new() -> Row {
    Row(vec![])
  }

  pub fn join<Separator: AsRef<str>>(&self, sep: Separator) -> String {
    let r: Vec<String> = self
      .0
      .iter()
      .map(|cell| {
        if let Ok(locked) = cell.clone().try_lock() {
          return format!("{}", locked.value_str());
        }
        String::new()
      })
      .collect();
    r.join(sep.as_ref())
  }

  pub fn push(&mut self, c: Cell) -> &Self {
    self.0.push(c);
    self
  }
  pub fn len(&self) -> usize {
    self.0.len()
  }

  pub fn iter(&self) -> std::slice::Iter<'_, Cell> {
    self.0.iter()
  }

  pub fn iter_mut(&mut self) -> std::slice::IterMut<'_, Cell> {
    self.0.iter_mut()
  }

  pub fn cell(&self, x: usize) -> Option<&Cell> {
    self.0.get(x)
  }

  pub fn cell_mut(&mut self, x: usize) -> Option<&mut Cell> {
    self.0.get_mut(x)
  }

  pub fn cells(&self) -> &Vec<Cell> {
    &self.0
  }

  pub fn cells_mut(&mut self) -> &mut Vec<Cell> {
    &mut self.0
  }

  pub fn cell_values(&self) -> Vec<Option<Value>> {
    self.0.iter().map(|v| v.try_lock_n(5).expect("failed to lock cell").value().clone()).collect()
  }
}

#[derive(Clone, Debug)]
pub struct Range {
  book_name: String,
  sheet_name: String,
  size: Size,
  header: Row,
  rows: Vec<Row>,
}

impl std::convert::From<Vec<Row>> for Range {
  fn from(mut rows: Vec<Row>) -> Range {
    let mut r = Range::new();
    match rows.len() {
      0 => {}
      1 => {
        r.header = rows[0].clone();
        r.size = Size {
          width: r.header.len(),
          height: 1,
        };
        r.rows = vec![];
      }
      y => {
        r.header = rows.remove(0);
        r.rows = rows.clone();
        r.size = Size {
          width: r.header.len(),
          height: y,
        };
      }
    }
    r.update_addresses();
    r
  }
}

impl std::convert::From<Vec<Vec<String>>> for Range {
  fn from(mut rows: Vec<Vec<String>>) -> Range {
    let mut r = Range::new();
    match rows.len() {
      0 => {}
      1 => {
        r.header = Row(
          rows[0]
            .iter()
            .map(|c| Cell::from_value(Value::Text(c.clone())))
            .collect(),
        );
        r.size = Size {
          width: r.header.len(),
          height: 1,
        };
        r.rows = vec![];
      }
      y => {
        r.header = Row(
          rows
            .remove(0)
            .iter()
            .map(|c| Cell::from_value(Value::Text(format!("{}", c))))
            .collect(),
        );
        r.rows = rows.iter().enumerate().map(|(y, mut inr)| {
          let mut r = Row::from(inr.clone());
          for (x, mut cell) in r.iter_mut().enumerate() {
            *cell.inner().address_mut() = Point::with_values(x, y);
          }
          r
        }).collect();
        r.size = Size {
          width: r.header.len(),
          height: y,
        };
      }
    }
    r.update_addresses();
    r
  }
}

impl Range {
  pub fn new() -> Range {
    Range {
      book_name: String::new(),
      sheet_name: String::new(),
      size: Size {
        width: 0,
        height: 0,
      },
      header: Row::new(),
      rows: vec![],
    }
  }

  pub fn with_values(bk: String, sh: String, sz: Size, hr: Row, r: Vec<Row>) -> Range {
    let mut r = Range {
      book_name: bk,
      sheet_name: sh,
      size: sz,
      header: hr,
      rows: r,
    };
    r.update_addresses();
    r
  }
  pub fn update_addresses(&mut self) {
    for (x, mut cell) in self.header.cells_mut().iter_mut().enumerate() {
      *cell.inner().address_mut() = Point::with_values(x, 0);
    }
    self.rows.iter_mut().enumerate().for_each(|(y, mut row)| {
      for (x, mut cell) in row.iter_mut().enumerate() {
        *cell.inner().address_mut() = Point::with_values(x, y + 1);
      }
    });
  }

  pub fn book_name(&self) -> &String {
    &self.book_name
  }

  pub fn sheet_name(&self) -> &String {
    &self.sheet_name
  }

  pub fn book_name_mut(&mut self) -> &mut String {
    &mut self.book_name
  }

  pub fn sheet_name_mut(&mut self) -> &mut String {
    &mut self.sheet_name
  }

  pub fn size(&self) -> &Size {
    &self.size
  }

  pub fn width(&self) -> usize {
    self.size.width
  }
  pub fn height(&self) -> usize {
    self.size.height
  }
  pub fn sub(&self, x: usize, y: usize, w: usize, h: usize) -> Result<Range> {
    if x + w >= self.width() || y + h >= self.height() {
      return Err(Error::Unknown(format!(
        "invalid range: {},{} -> {},{}",
        x,
        y,
        x + w,
        y + h
      )));
    }
    let mut ret = Range::with_values(
      self.book_name.clone(),
      self.sheet_name.clone(),
      self.size,
      self.header.clone(),
      vec![],
    );
    for row in &self.rows[y..y + h] {
      ret.push_row(Row((*row.cells())[x..x + w].to_vec()));
    }
    ret.update_addresses();
    Ok(ret)
  }

  pub fn size_mut(&mut self) -> &mut Size {
    &mut self.size
  }
  pub fn header(&self) -> &Row {
    &self.header
  }
  pub fn rows(&self) -> &Vec<Row> {
    &self.rows
  }
  pub fn header_mut(&mut self) -> &mut Row {
    &mut self.header
  }
  pub fn rows_mut(&mut self) -> &mut Vec<Row> {
    &mut self.rows
  }
  pub fn column_index(&self, n: &String) -> Result<Option<u32>> {
    let item = self
      .header
      .iter()
      .enumerate()
      .filter(|(i, h)| {
        if let Ok(l) = h.try_lock_n(5) {
          if (l.value_str() == *n) {
            return true;
          }
        }
        return false;
      })
      .map(|(i, h)| i as u32)
      .collect::<Vec<u32>>();
    if !item.is_empty() {
      return Ok(Some(*item.first().unwrap()));
    }
    Ok(None)
  }

  pub fn cell(&self, x: u32, y: u32) -> Option<&Cell> {
    if let Some(r) = self.rows.get(y as usize) {
      return r.cell(x as usize);
    }
    None
  }

  pub fn push_row(&mut self, r: Row) {
    let y = self.rows.len();
    self.rows.push(r);
    self.size.height += 1;
    let mut row = self.rows.last_mut().unwrap();
    for x in 0..row.cells().len() {
      *row.cell(x).unwrap().inner().address_mut() = Point{y, x}
    }
  }

  pub fn set_header(&mut self, r: Row) {
    self.header = r;
    self.size.width = self.header.len();
  }

  pub fn filter(&self, f: Filter) -> Result<Range> {
    use log::trace;
    let mut r = Range::new();
    r.book_name = self.book_name.clone();
    r.sheet_name = self.sheet_name.clone();
    r.size = self.size;
    r.size.height = 0;
    r.header = self.header.clone();
    use std::ops::Deref;
    let tokens = f.expr().data();
    let mut token: &Token;
    let mut row: &Row;
    let mut kept = true;
    let mut op: Token = Token::And;
    let mut field_name: String = String::new();
    let mut value: String = String::new();
    log::trace!("filter: {:?}", f);
    for rid in 0..self.rows.len() {
      op = Token::And;
      row = self.rows.get(rid).unwrap();
      kept = true;
      for i in 0..tokens.len() {
        token = tokens.get(i).unwrap();
        match token {
          Token::Or => op = Token::Or,
          Token::And => op = Token::And,
          Token::Equals => {
            value.clear();
            field_name.clear();
            match tokens
              .get(i - 1)
              .ok_or(Error::Unknown(format!("missing left hand side")))?
            {
              Token::Ident(v) => field_name = v.clone(),
              v => {
                return Err(Error::Unknown(format!(
                  "{}: invalid value, expected identifier",
                  v
                )))
              }
            }
            match tokens
              .get(i + 1)
              .ok_or(Error::Unknown(format!("missing right hand side")))?
            {
              Token::Ident(v) => value = v.clone(),
              v => {
                return Err(Error::Unknown(format!(
                  "{}: invalid value, expected identifier",
                  v
                )))
              }
            }
            let col = self
              .column_index(&field_name)
              .or_else(|e| Err(Error::Unknown(e.to_string())))?
              .ok_or(Error::Unknown(format!(
                "unknown column '{}', available: {}",
                field_name,
                r.header.join(", ")
              )))?;
            let old_kept = kept;
            let cell = self.cell(col, rid as u32).unwrap();
            let local_cell = cell
              .try_lock()
              .or_else(|e| Err(Error::Unknown(e.to_string())))?;
            match op {
              Token::And => kept = kept && (value == "*" || local_cell.value_str() == value),
              Token::Or => kept = kept || (value == "*" || local_cell.value_str() == value),
              _ => log::warn!("Unhandled operator '{}'", op)
            }
            trace!(
              "(keep_predicate) {} {} {} {} {} on '{}' -> {}",
              old_kept,
              op,
              field_name,
              Token::Equals,
              value,
              row.join(", "),
              kept
            );
          }
          t => {}
        }
      }
      if kept {
        r.push_row(row.clone());
      }
    }
    r.update_addresses();
    Ok(r)
  }
}

#[cfg(test)]
mod tests {
  use super::*;

  #[test]
  fn range_should_allow_filtering_of_simple_conditions() {
    let r = Range::with_values(
      String::new(),
      String::new(),
      Size {
        width: 1,
        height: 1,
      },
      vec![String::from("a")],
      vec![vec!["1".to_string()], vec!["0".to_string()]],
    );
    let o = r
      .filter(Filter::parse("a = 1").expect("failed to parse filter"))
      .expect("failed to apply filter");
    assert_eq!(r.size, o.size);
    assert_eq!(r.header, o.header);
    assert_eq!(o.rows, vec![vec!["1".to_string()]]);
  }

  #[test]
  fn range_should_allow_filtering_of_boolean_conditions() {
    let r = Range::with_values(
      String::new(),
      String::new(),
      Size {
        width: 2,
        height: 2,
      },
      vec!["a".to_string(), "b".to_string()],
      vec![
        vec!["1".to_string(), "1".to_string()],
        vec!["0".to_string(), "1".to_string()],
        vec!["1".to_string(), "0".to_string()],
        vec!["-1".to_string(), "0".to_string()],
      ],
    );
    let mut o = r
      .filter(Filter::parse("a = 1 && b = 1").expect("failed to parse filter"))
      .expect("failed to apply filter");
    assert_eq!(o.rows, vec![vec!["1".to_string(), "1".to_string()]]);

    o = r
      .filter(Filter::parse("a = 1 || b = 1").expect("failed to parse filter"))
      .expect("failed to apply filter");
    assert_eq!(
      o.rows,
      vec![
        vec!["1".to_string(), "1".to_string()],
        vec!["0".to_string(), "1".to_string()],
        vec!["1".to_string(), "0".to_string()],
      ]
    );

    o = r
      .filter(Filter::parse("a = -1 || b = 0").expect("failed to parse filter"))
      .expect("failed to apply filter");
    assert_eq!(
      o.rows,
      vec![
        vec!["1".to_string(), "0".to_string()],
        vec!["-1".to_string(), "0".to_string()],
      ]
    );
  }
}
