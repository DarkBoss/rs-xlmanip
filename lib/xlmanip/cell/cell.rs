pub use crate::coords::*;
pub use crate::error::Error;
pub use xlsxwriter as xlw;

#[derive(Clone, Debug, PartialEq)]
pub enum Value {
    Text(String),
    Formula(String),
    Number(f64),
}

impl std::fmt::Display for Value {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Value::Text(v) => v.clone(),
                Value::Formula(v) => v.clone(),
                Value::Number(v) => format!("{}", v),
            }
        )
    }
}

#[derive(Clone)]
pub struct CellInner {
    pub name: Option<String>,
    pub address: Point,
    pub alignment: Option<xlw::FormatAlignment>,
    pub border: Option<xlw::FormatBorder>,
    pub color: Option<xlw::FormatColor>,
    pub patterns: Option<xlw::FormatPatterns>,
    pub script: Option<xlw::FormatScript>,
    pub value: Option<Value>,
}

impl CellInner {
    pub fn new(name: Option<String>) -> CellInner {
        CellInner {
            name: name,
            address: Point::new(),
            alignment: None,
            border: None,
            color: None,
            patterns: None,
            script: None,
            value: None,
        }
    }

    pub fn x(&self) -> usize {
        self.address.x
    }
    pub fn y(&self) -> usize {
        self.address.y
    }

    pub fn x_mut(&mut self) -> usize {
        self.address.x
    }
    pub fn y_mut(&self) -> usize {
        self.address.y
    }

    pub fn has_value(&self) -> bool {
        self.value.is_some()
    }

    pub fn has_alignment(&self) -> bool {
        self.alignment.is_some()
    }
    pub fn has_border(&self) -> bool {
        self.border.is_some()
    }
    pub fn has_color(&self) -> bool {
        self.color.is_some()
    }
    pub fn has_script(&self) -> bool {
        self.script.is_some()
    }
    pub fn has_patterns(&self) -> bool {
        self.patterns.is_some()
    }
    pub fn is_text(&self) -> bool {
        if self.has_value() {
            if let Value::Text(v) = self.value.as_ref().unwrap() {
                return true;
            }
        }
        false
    }
    pub fn is_number(&self) -> bool {
        if self.has_value() {
            if let Value::Number(v) = self.value.as_ref().unwrap() {
                return true;
            }
        }
        false
    }

    pub fn is_formula(&self) -> bool {
        if self.has_value() {
            if let Value::Formula(v) = self.value.as_ref().unwrap() {
                return true;
            }
        }
        false
    }

    pub fn alignment(&self) -> &Option<xlw::FormatAlignment> {
        &self.alignment
    }

    pub fn alignment_mut(&mut self) -> &mut Option<xlw::FormatAlignment> {
        &mut self.alignment
    }

    pub fn border(&self) -> &Option<xlw::FormatBorder> {
        &self.border
    }

    pub fn border_mut(&mut self) -> &mut Option<xlw::FormatBorder> {
        &mut self.border
    }

    pub fn color(&self) -> &Option<xlw::FormatColor> {
        &self.color
    }

    pub fn color_mut(&mut self) -> &mut Option<xlw::FormatColor> {
        &mut self.color
    }

    pub fn patterns(&self) -> &Option<xlw::FormatPatterns> {
        &self.patterns
    }

    pub fn patterns_mut(&mut self) -> &mut Option<xlw::FormatPatterns> {
        &mut self.patterns
    }

    pub fn script(&self) -> &Option<xlw::FormatScript> {
        &self.script
    }

    pub fn script_mut(&mut self) -> &mut Option<xlw::FormatScript> {
        &mut self.script
    }

    pub fn value(&self) -> &Option<Value> {
        &self.value
    }

    pub fn value_str(&self) -> String {
        match &self.value {
            Some(v) => v.to_string(),
            None => format!("{}", "None"),
        }
    }

    pub fn value_mut(&mut self) -> &mut Option<Value> {
        &mut self.value
    }

    pub fn address(&self) -> &Point {
        &self.address
    }

    pub fn address_mut(&mut self) -> &mut Point {
        &mut self.address
    }
}

pub type CellMutex = std::sync::Mutex<CellInner>;

#[derive(Clone)]
pub struct Cell(std::sync::Arc<CellMutex>);

impl std::cmp::Eq for Cell {
}

impl std::cmp::PartialEq<Cell> for Cell {
    fn eq(&self, other: &Cell) -> bool {
        self.try_lock().expect("failed to lock cell").value_str()
            == other.try_lock().expect("failed to lock cell").value_str()
    }
}

impl std::fmt::Debug for Cell {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let locked = self.try_lock_n(5).expect("failed to lock cell").clone();
        write!(
            f,
            "Cell<{}x{}> = {}",
            locked.address().x,
            locked.address().y,
            match locked.value() {
                Some(v) => format!("{}", v),
                None => format!("None"),
            }
        )
    }
}

impl Cell {
    pub fn inner(&self) -> std::sync::MutexGuard<'_, CellInner> {
        self.try_lock_n(5).expect("failed to lock cell")
    }

    pub fn try_lock(&self) -> crate::result::Result<std::sync::MutexGuard<'_, CellInner>> {
        self.0
            .try_lock()
            .or_else(|e| Err(Error::Unknown(e.to_string())))
    }

    pub fn try_lock_n(&self, n: u8) -> crate::result::Result<std::sync::MutexGuard<'_, CellInner>> {
        for i in 0..10 {
            if let Ok(r) = self.0.try_lock() {
                return Ok(r);
            }
        }
        return Err(Error::Unknown(format!("{}", "failed to lock cell")));
    }

    pub fn lock(&self) -> crate::result::Result<std::sync::MutexGuard<'_, CellInner>> {
        self.0
            .lock()
            .or_else(|e| Err(Error::Unknown(e.to_string())))
    }

    pub fn new(name: Option<String>) -> Cell {
        Cell::with_values(CellInner::new(None))
    }

    pub fn with_values(v: CellInner) -> Cell {
        Cell(std::sync::Arc::new(std::sync::Mutex::new(v)))
    }

    pub fn from_value(v: Value) -> Cell {
        let mut data = CellInner::new(None);
        *data.value_mut() = Some(v);
        Cell::with_values(data)
    }
}
