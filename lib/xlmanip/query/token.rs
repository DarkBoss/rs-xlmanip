use super::expr::Expr;

#[derive(Debug, Clone, PartialEq, PartialOrd, Ord, Eq)]
pub enum Token {
  And,
  Or,
  Equals,
  Ident(String),
  Expr(Expr),
  Unparsed(String),
}

impl Token {
  pub fn name(&self) -> String {
    match self {
      Token::And => "and",
      Token::Or => "or",
      Token::Equals => "equals",
      Token::Ident(_) => "ident",
      Token::Expr(_) => "expr",
      Token::Unparsed(_) => "unparsed",
    }
    .to_string()
  }

  pub fn symbol(&self) -> String {
    match self {
      Token::And => "&&".to_string(),
      Token::Or => "||".to_string(),
      Token::Equals => "=".to_string(),
      Token::Ident(s) => s.to_string(),
      Token::Expr(e) => format!("{}", e),
      Token::Unparsed(s) => s.to_string(),
    }
  }
}

impl std::fmt::Display for Token {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(
      f,
      "{}",
      self.symbol()
    )
  }
}
