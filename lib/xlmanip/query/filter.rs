use crate::result::Result;
use crate::error::Error;

use super::expr::{Expr, EXPR_CHARSET};
use super::token::Token;
use crate::book::Book;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Filter(Expr);

impl Filter {
  pub fn new() -> Filter {
    Filter(Expr::new())
  }

  pub fn with_expr(e: Expr) -> Filter {
    Filter(e)
  }

  pub fn expr(&self) -> &Expr {
    &self.0
  }

  pub fn expr_mut(&mut self) -> &mut Expr {
    &mut self.0
  }

  pub fn parse<S: AsRef<str>>(s: S) -> Result<Filter> {
    Ok(Filter::from(Expr::parse(s.as_ref().to_string())?))
  }

  // pub fn map_row(&self, row: &calamine::Cells.) -> HashMap<String, calamine::Cell> {
  // }

  pub fn apply(&self, book: &mut Book) -> Result<()> {
    Ok(())
  }
}

impl std::convert::From<Expr> for Filter {
  fn from(e: Expr) -> Filter {
    Filter::with_expr(e)
  }
}


impl std::convert::From<String> for Filter {
  fn from(s: String) -> Filter {
    Filter::with_expr(Expr::parse(s).expect("failed to parse"))
  }
}