#[derive(Clone)]
pub struct Iter {
  content: String,
  offset: usize,
  line: usize,
  column: usize
}

impl std::convert::From<String> for Iter {
  fn from(s: String) -> Iter {
    Iter {
      content: s,
      offset: 0,
      line: 1,
      column: 1
    }
  }
}

impl Iterator for Iter {
  type Item = char;
  
  fn next(&mut self) -> Option<Self::Item> {
    if self.offset >= self.content.len() {
      return None;
    }
    if self.offset < self.content.len() {
      if let Some(ch) = self.peek() {
        self.offset += 1;
        if ch == '\n' {
          self.line += 1;
          self.column = 1;
        } else {
          self.column += 1;
        }
        return Some(ch);
      }
    }
    None    
  }
}

impl Iter {
  pub fn peek(&self) -> Option<char> {
    if self.offset >= self.content.len() {
      None
    } else {
      self.content.chars().nth(self.offset)
    }
  }

  pub fn offset(&self) -> usize {
    self.offset
  }

  pub fn line(&self) -> usize {
    self.line
  }

  pub fn column(&self) -> usize {
    self.column
  }
}