use super::context::Context;
use super::location::Location;

#[derive(Clone, Debug)]
pub struct SyntaxError {
  location: Location,
  message: String,
  content: String,
}

impl SyntaxError {
  pub fn location(&self) -> &Location { &self.location }
  pub fn message(&self) -> &String { &self.message }
  pub fn content(&self) -> &String { &self.content }
}

impl std::convert::From<Context> for SyntaxError {
  fn from(c: Context) -> SyntaxError {
    SyntaxError {
      message: String::new(),
      location: c.location,
      content: c.line_accu,
    }
  }
}