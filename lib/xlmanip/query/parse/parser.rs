use super::iter::*;
use super::context::*;
use super::super::token::Token;
use super::super::expr::Expr;
use crate::result::Result;
use crate::error::Error;

pub struct Parser {}

impl Parser {
  pub fn new() -> Parser {
    Parser{}
  }

  pub fn parse(&mut self, s: String) -> Result<Expr> {
    self._parse(Iter::from(s), Context::default())
  }

  fn _parse(&mut self, mut iter: Iter, mut ctx: Context) -> Result<Expr> {
    let mut triggers = std::collections::HashMap::<char, Box<dyn Fn(char) -> Result<()>>>::new();
    triggers.insert(' ', Box::new(|ch| -> Result<()> {
      Ok(())
    }));
    while let Some(ch) = iter.peek() {
      match ch {
        '=' => {
          if ctx.in_quote {
            ctx.add_char_to_accu(ch);
          } else {
            if !ctx.accu.is_empty() {
              ctx.add_token_to_expr(Token::Ident(ctx.accu.clone()));
            }
            ctx.add_token_to_expr(Token::Equals);
          }
          iter.next();
        },
        '(' => {
          if !ctx.in_quote {
            if !ctx.accu.is_empty() {
              ctx.add_token_to_expr(Token::Ident(ctx.accu.clone()));
            }
            iter.next();
            let sub_expr = self._parse(iter, ctx.sub_scope())?;
            if !sub_expr.is_empty() {
              ctx.add_token_to_expr(Token::Expr(sub_expr));
            }
            break
          } else {
            ctx.add_char_to_accu(ch);
            iter.next();
          }
        },
        ')' => {
          if !ctx.in_quote {
            if !ctx.accu.is_empty() {
              ctx.add_token_to_expr(Token::Ident(ctx.accu.clone()));
            }
            if ctx.scope == 0 {
              return Err(Error::Unknown(format!("unmatched closing parenthesis in '{}'", iter.collect::<String>())));
            }
            ctx.scope -= 1;
            break
          } else {
            ctx.accu.push(ch);
          }
          iter.next();
        },
        ' ' | '\t' => {
          if ctx.in_quote {
            ctx.accu.push(ch);
          }
          iter.next();
        }
        '&' => {
          if !ctx.in_quote {
            if !ctx.accu.is_empty() {
              ctx.add_token_to_expr(Token::Ident(ctx.accu.clone()));
            }
            if let Some(next) = iter.next() {
              if next == '&' {
                ctx.expr.push_token(Token::And);
              }
            }
          } else {
            ctx.accu.push(ch);
          }
          iter.next();
        }
        '|' => {
          if !ctx.in_quote {
            if !ctx.accu.is_empty() {
              ctx.add_token_to_expr(Token::Ident(ctx.accu.clone()));
            }
            if let Some(next) = iter.next() {
              if next == '|' {
                ctx.expr.push_token(Token::Or);
              }
            }
          } else {
            ctx.accu.push(ch);
          }
          iter.next();
        }
        '"' | '\'' => {
          ctx.in_quote = !ctx.in_quote;
          if !ctx.in_quote {
            ctx.expr.push_token(Token::Ident(ctx.accu.clone()));
            ctx.accu.clear();
          }
          iter.next();
        }
        c => {
          if ctx.in_quote || Expr::charset_contains(c) {
            ctx.accu.push(c);
          } else {
            return Err(Error::Unknown(format!("invalid expression character '{}'", c)))
          }
          iter.next();
        }
      };
    }

    if ctx.in_quote {
      return Err(Error::Unknown(format!("missing ending quote")));
    }

    if !ctx.accu.is_empty() {
      ctx.expr.push_token(Token::Ident(ctx.accu.clone()));
      ctx.accu.clear();
    }

    Ok(self.validate(ctx.expr)?)
  }

  pub fn validate(&self, e: Expr) -> Result<Expr> {
    if e.is_empty() {
      return Ok(e);
    }
    match e.first().unwrap() {
      Token::And => return Err(Error::Unknown(format!("invalid operator at start of expression"))),
      Token::Or => return Err(Error::Unknown(format!("invalid operator at start of expression"))),
      _ => {}
    }
    use std::ops::Deref;
    use log::warn;
    for tok in e.deref() {
      if let Token::Unparsed(unparsed) = tok {
        warn!("{}: skipped token '{}'", "", unparsed);
      }
    }
    // enum JoinType {
    //   Before,
    //   After,
    //   Both
    // }
    // let join_priorities = vec![
    //   (Token::Equals, JoinType::Both),
    // ];
    // let mut data = e.data().clone();
    // let mut tok: &Token;
    // for prio in join_priorities {
    //   for i in 0..data.len() {
    //     tok = data.get(i).unwrap();
    //     match tok {
    //       c if *tok == prio.0 => {
    //         let mut sub = Expr::new();
    //         match prio.1 {
    //           JoinType::After => {
    //             sub.push_token(tok.clone());
    //             if i + 1 >= data.len() {
    //               return Err(Error::Unknown(format!("missing right hand side for token '{}'", tok)));
    //             }
    //             sub.push_token(data.get(i + 1).unwrap().clone());
    //           }
    //           JoinType::Before => {
    //             sub.push_token(tok.clone());
    //             if i - 1 >= data.len() {
    //               return Err(Error::Unknown(format!("missing left hand side for token '{}'", tok)));
    //             }
    //             sub.push_token(data.get(i - 1).unwrap().clone());
    //           }
    //           JoinType::Both => {
    //             sub.push_token(tok.clone());
    //             if i - 1 >= data.len() {
    //               return Err(Error::Unknown(format!("missing left hand side for token '{}'", tok)));
    //             }
    //             sub.push_token(data.get(i - 1).unwrap().clone());
    //             if i + 1 >= data.len() {
    //               return Err(Error::Unknown(format!("missing right hand side for token '{}'", tok)));
    //             }
    //             sub.push_token(data.get(i + 1).unwrap().clone());
    //           }
    //         }
    //       }
    //       _ => {}
    //     }
    //   }
    // }
    // Ok(Expr::from(data))
    Ok(e)
  }

}


#[cfg(test)]
mod tests {
  use super::*;
  #[test]
  fn parser_should_parse_single_field() {
    let f = Parser::new().parse("test = 42".to_string()).expect("failed to parse");
    assert_eq!(f, Expr::from(vec![
      Token::Ident("test".to_string()),
      Token::Equals,
      Token::Ident("42".to_string())
    ]));
    println!("{:?}", f);
  }

  #[test]
  fn parser_should_parse_operator_and() {
    let f = Parser::new().parse("a && b && c".to_string()).expect("failed to parse");
    assert_eq!(f, Expr::from(vec![
      Token::Ident("a".to_string()),
      Token::And,
      Token::Ident("b".to_string()),
      Token::And,
      Token::Ident("c".to_string())
    ]));
    println!("{:?}", f);
  }

  #[test]
  fn parser_should_parse_operator_or() {
    let f = Parser::new().parse("a || b || c".to_string()).expect("failed to parse");
    assert_eq!(f, Expr::from(vec![
      Token::Ident("a".to_string()),
      Token::Or,
      Token::Ident("b".to_string()),
      Token::Or,
      Token::Ident("c".to_string())
    ]));
    println!("{:?}", f);
  }

  #[test]
  fn parser_should_parse_operator_mixed() {
    let f = Parser::new().parse("a && b = 2 || c".to_string()).expect("failed to parse");
    assert_eq!(f, Expr::from(vec![
      Token::Ident("a".to_string()),
      Token::And,
      Token::Ident("b".to_string()),
      Token::Equals,
      Token::Ident("2".to_string()),
      Token::Or,
      Token::Ident("c".to_string())
    ]));
    println!("{:?}", f);
  }

  #[test]
  fn parser_should_parse_object_notation() {
    let f = Parser::new().parse("obj.field = 42".to_string()).expect("failed to parse");
    assert_eq!(
      f,
      Expr::from(vec![
        Token::Ident("obj.field".to_string()),
        Token::Equals,
        Token::Ident("42".to_string())
      ])
    );
    println!("{:?}", f);
  }

  #[test]
  fn parser_should_parse_quotes() {
    let f = Parser::new().parse("obj.field = \"val\"".to_string()).expect("failed to parse");
    assert_eq!(
      f,
      Expr::from(vec![
        Token::Ident("obj.field".to_string()),
        Token::Equals,
        Token::Ident("val".to_string())
      ])
    );
    println!("{:?}", f);
  }

  #[test]
  fn parser_should_parse_parenthesis() {
    let f = Parser::new().parse("a || (b = 2)".to_string()).expect("failed to parse");
    println!("{:#?}", f);
    assert_eq!(
      f,
      Expr::from(vec![
        Token::Ident("a".to_string()),
        Token::Or,
        Token::Expr(Expr::from(vec![
          Token::Ident("b".to_string()),
          Token::Equals,
          Token::Ident("2".to_string())
        ])),
      ])
    );
    println!("{:?}", f);
  }
}