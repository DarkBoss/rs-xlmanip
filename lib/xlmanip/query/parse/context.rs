use super::super::expr::Expr;
use super::super::token::Token;
use super::location::Location;

#[derive(Clone)]
pub struct Context {
  pub expr: Expr,
  pub location: Location,
  pub scope: usize,
  pub accu: String,
  pub line_accu: String,
  pub in_quote: bool,
}

impl std::default::Default for Context
{
  fn default() -> Context {
    Context {
      expr: Expr::new(),
      location: Location::new(),
      scope: 0,
      accu: String::new(),
      line_accu: String::new(),
      in_quote: false
    }
  }
}

impl Context {
  pub fn sub_scope(&self) -> Self {
    let mut ctx = self.clone();
    ctx.expr = Expr::new();
    ctx.scope += 1;
    ctx.accu.clear();
    ctx
  }

  pub fn add_char_to_accu(&mut self, ch: char) {
    self.accu.push(ch);
    
  }

  pub fn add_token_to_expr(&mut self, t: Token) {
    self.expr.push_token(t);
    self.accu.clear();
  }
}