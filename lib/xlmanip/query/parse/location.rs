#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Location {
  source: Option<String>,
  offset: usize,
  line: usize,
  column: usize,
}

impl std::fmt::Display for Location {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{}{}:{}", match &self.source {
      Some(s) => format!("{}:", s),
      None => format!("")
    }, self.line, self.column)
  }
}
impl Location {
  pub fn new() -> Location {
    Location {
      source: None,
      offset: 0,
      line: 1,
      column: 1,
    }
  }
}