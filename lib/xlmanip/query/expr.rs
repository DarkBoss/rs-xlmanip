use crate::error::Error;
use crate::result::Result;
use super::token::Token;
use super::filter::Filter;
use super::parse::Parser;

pub const EXPR_CHARSET: &'static str = "_-.-=()*";

#[derive(Debug, Clone, PartialEq, PartialOrd, Ord, Eq)]
pub struct Expr(Vec<Token>);

use std::ops::Deref;
impl Deref for Expr {
  type Target = Vec<Token>;

  fn deref(&self) -> &Self::Target {
      &self.0
  }
}

use std::ops::DerefMut;
impl DerefMut for Expr {
  // type Target = Vec<Token>;

  fn deref_mut(&mut self) -> &mut Self::Target {
      &mut self.0
  }
}

impl std::fmt::Display for Expr {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    let mut s = String::new();
    for tok in self.deref() {
      if !s.is_empty() {
        s.push(' ');
      }
      s = format!("{}{}", s, tok.symbol());
    }
    write!(f, "{}", s)
  }
}

impl Expr {
  pub fn new() -> Expr {
    Expr(vec![])
  }
  
  pub fn charset_contains(ch: char) -> bool {
    Self::is_identifier(ch) || EXPR_CHARSET.find(ch).is_some()
  }

  pub fn is_identifier(ch: char) -> bool {
    ch.is_alphanumeric()
  }


  pub fn parse(s: String) -> Result<Expr> {
    Parser::new().parse(s)
  }
    
  pub fn or(&mut self, e: Expr) -> Result<&mut Self> {
    self.chain(Token::Or, e)
  }
    
  pub fn and(&mut self, e: Expr) -> Result<&mut Self> {
    self.chain(Token::And, e)
  }

  pub fn chain(&mut self, op: Token, e: Expr) -> Result<&mut Self> {
    if e.is_empty() {
      return Err(Error::Unknown(format!("cannot add '{}' condition, expression is empty", op.name())))
    }
    match op {
      Token::And => {},
      Token::Or => {},
      _ => return Err(Error::Unknown(format!("invalid chaining operator '{}'", op.name())))
    }
    if !self.is_empty() {
      self.push_token(op);
    }
    self.push_token(Token::Expr(e));
    Ok(self)
  }

  pub fn push_token(&mut self, t: Token) -> &mut Self {
    self.0.push(t);
    self
  }

  pub fn pop_token(&mut self) -> Option<Token> {
    self.0.pop()
  }

  pub fn data(&self) -> &Vec<Token> { &self.0 }
  pub fn data_mut(&mut self) -> &mut Vec<Token> { &mut self.0 }

  pub fn get(&self, i: usize) -> Option<&Token> { self.0.get(i) }
  pub fn get_mut(&mut self, i: usize) -> Option<&mut Token> { self.0.get_mut(i) }

  pub fn is_empty(&self) -> bool {  self.0.is_empty() }
  pub fn len(&self) -> usize { self.0.len() }

  pub fn iter(&self) -> impl Iterator<Item=&Token> { self.0.iter() }
  pub fn into_iter(self) -> impl IntoIterator<Item=Token> { self.0.into_iter() }

}

pub struct Iter<'a> {
  expr: &'a Expr,
  cur: usize,
  max: usize
}

impl<'a> Iter<'a> {
  pub fn new(expr: &'a Expr) -> Iter {
    Iter {
      expr,
      cur: 0,
      max: expr.len()
    }
  }
}

impl<'a> Iterator for Iter<'a> {
  type Item = &'a Token;

  fn next(&mut self) -> Option<Self::Item> {
    if self.cur >= self.max {
      return None;
    }
    let ret = self.expr.get(self.cur);
    self.cur += 1;
    ret
  }
}

impl std::convert::From<Vec<Token>> for Expr {
  fn from(v: Vec<Token>) -> Expr {
    let mut e = Expr::new();
    e.0.extend(v.into_iter());
    e
  }
}
