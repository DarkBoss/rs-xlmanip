use crate::error::Error;
use crate::result::Result;

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Ord, Eq)]
pub enum Element {
  Book,
  Sheet,
  Cell,
  Formula
}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Ord, Eq)]
pub enum Op {
  And,
  Or,
  None,
}

#[derive(Debug, Clone, PartialEq, PartialOrd, Ord, Eq)]
pub enum Expr {
  One(Element, Op, Element),
  List(Vec<Expr>)
}

impl std::default::Default for Expr {
  fn default() -> Expr {
    Expr::List(vec![])
  }
}

#[derive(Debug, Clone, PartialEq, PartialOrd, Ord, Eq)]
pub struct SelectQuery {
  fields: Vec<String>,
  sheet: Option<String>,
  book: Option<String>,
  conditions: Option<String>,
  orig: String,
}

impl SelectQuery {
  pub fn parse<S: AsRef<str>>(s: S) -> Result<SelectQuery> {
    use regex::Regex;
    let chain: Vec<Expr> = vec![];
    let mut q = SelectQuery{
      fields: vec![],
      sheet: None,
      book: None,
      conditions: None,
      orig: s.as_ref().to_string(),
    };
    let cur: Option<Expr> = None;
    let re = Regex::new(r"select\s+([\w,\s]+|\*)\s+from\s+([\.\w]+)\s*([.;]*)").unwrap();
    for cap in re.captures_iter(s.as_ref()) {
      let fields = &cap[1];
      let sheet = &cap[2];
      let conditions = &cap[3];
      for field in fields.split(",") {
        q.fields.push(field.to_string());
      }
      if conditions.len() > 0 {
        q.conditions = Some(conditions.to_string());
      }
      println!("found: {} {} {}", fields, sheet, conditions);
    }
    Ok(q)
  }
}

impl std::fmt::Display for SelectQuery {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(f, "{}", self.orig)
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  #[test]
  fn query_select_from_all_books() {
    let q = SelectQuery::parse("select * from test").expect("failed to parse");
    println!("{}", q);
    println!("{:#?}", q)
  }

  #[test]
  fn query_select_from_specific_book() {
    let q = SelectQuery::parse("select * from book_b.test").expect("failed to parse");
    println!("{}", q);
    println!("{:#?}", q)
  }
}