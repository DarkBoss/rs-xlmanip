extern crate xlmanip;

use xlmanip::*;

fn try_main() -> Result<()> {
  init_logging()?;
  parse_options()?;
  Ok(App::new().run()?)
}

fn main() {
  if let Err(e) = try_main() {
    panic!("error: {}", e.to_string());
  }
}
