# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.0] - 2020-01-31
### Added
This release marks the point where the base system is in place.
The list-rows app mode makes use of it to allow filtering and csv output.

Needs refactoring though.